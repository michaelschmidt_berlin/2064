<div class="impressum">
    <strong>Bitbucket</strong>: <a href="${BOOK_REPOSITORY}">michaelschmidt_berlin/2064</a><br />
    (Beiträge erwünscht ...)<br />
    <strong>ASIN</strong>: ${BOOK_ASIN}<br />
    <strong>ISBN</strong>: ${BOOK_ISBN}<br />
    <strong>Version</strong>: ${BOOK_VERSION}.${BITBUCKET_BUILD_NUMBER}.${BITBUCKET_COMMIT}<br />
    <strong>Lizenz</strong>: ${BOOK_LICENSE}<br />
    <strong>Mitwirkende</strong>:<br />
    Volker Diels-Grabsch<br />
    Maarja Urb<br />
    Benedicte Walentin Moe<br />
    Christian 'Crille' Vandrei<br />
    Rainer-Maria Fritsch<br />
</div>

<div class="title-page">Für Julian Assange</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="title-large">Teil 1</div>
<div style="visibility: hidden;">\pagebreak</div>

## Das Spiele-Labor 
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Es war die</span> schönste Zeit des Jahres. Lasse schlenderte voller Freude auf das Spiele-Labor zu, das in der Mitte des Schulgeländes stand, wo der Bach sich zweigte und auf beiden Seiten um das Labor herum floss.
Er sah sich um, betrachtete im Vorbeigehen die völlig unterschiedlich gebauten Häuser, aus Lehm, Holz und großen Steinen, mit fantasievoll geformten Fenstern und bunten Dächern.
Er mochte es, an ihnen immer wieder ein neues Detail zu entdecken.
Und man konnte den Häusern wirklich ansehen, wer darin arbeitete und wohnte.
Er lächelte.

Am Eselgehege wartete Alfred schon und nickte ihm gemütlich mit seiner langen Schnauze zu.
Lasse zog eine dicke Karotte aus seiner Tasche und streckte sie ihm entgegen.
Der schnappte sie vorsichtig aus seiner Hand und drehte sich damit um.

Zur Insel des Spiele-Labors gab es keine Brücke.
Um hineinzukommen, musste man über den Bach springen und Lasse suchte sich dafür eine Stelle aus, wo er es gerade so schaffte.
Vor dem Haus warf er einen kurzen Blick auf das Schloss der Tür und sie sprang mit einem leisen Klick auf.

Es war neun Uhr, eine halbe Stunde vor Beginn des dritten Spieltages.
Er liebte die TRON-Wochen, ein riesiges Computerspielturnier, bei dem Tausende von Spielern aus vielen Teilen der Welt in einer virtuellen Computerwelt mit- und gegeneinander um die Weltherrschaft spielten.
Und nicht in einer frei erfundenen Welt, sondern in einem ziemlich originalgetreuen Nachbau des Internets der Jahre 2023 bis 2033, einer grandiosen Simulation mit unendlich vielen realistischen Details aus dieser Zeit.
Er hatte sich zusammen mit Freunden über Monate darauf vorbereitet.

Ein Ziel war es, Missionen zu erfüllen, zum Beispiel Computer zu erobern, sie unter Kontrolle zu bekommen und zu verteidigen, in Banken, in Firmen oder auch bei irgendjemanden zu Hause, der etwas Interessantes machte.
Man konnte auch Satelliten, Schiffe oder Flugzeuge übernehmen, Agenten und Hacker enttarnen.
Am meisten Punkte gab es, wenn man an geheime Dokumente kam, Journale von geheimen Kriegen, geheime Abmachungen zwischen Unternehmen, Geheimoperationen von Regierungen.
Man konnte sie auch teuer verkaufen.

Eine sehr beliebte, aber auch schwierige Mission war es, bekannte Hacker wie <span class="person">Kevin Mitnick</span>, <span class="person">Adrian Lamo</span> oder Julian Assange zu jagen.
Lasse hatte unbedingt selbst <span class="person">Julian Assange</span> spielen wollen, aber das wollten wohl zu viele andere auch, und wohl deswegen war er ein weniger bekannter Hacker geworden, mit der Mission Satelliten, fliegende Kampfroboter und andere Waffen in einem Kriegsgebiet zu übernehmen.
Man musste dafür einiges über Computersysteme wissen, auch die politische Lage von damals kennen und wie die Menschen dachten und fühlten.

Damals herrschte Krieg im Internet und in vielen Teilen der Welt.
Die Welt war voller schrecklicher Konflikte zwischen Ländern und Unternehmen, in Regierungen, überall. Selbst in Schulen und Familien gab es viele Konflikte.
Es war eine völlig andere Welt.
Da mussten sich die Schüler hineindenken.

Lasse betrat das Spielzimmer, in dem Sigur schon gebannt vor seinem Rechner saß und ab und zu etwas tippte.
Lasse: „Hej, moin, moin!“
Er schlug ihm im Vorbeigehen mit der Hand auf die Schulter und ließ sich in seinen Stuhl fallen.

Sigur war sein Flügelmann beim Spiel, sein Partner in der aktuellen Mission.
Er war auch sein Freund, wenn auch nicht der beste.
„Er macht zu oft sein eigenes Ding“, dachte Lasse, „hat zu genaue Vorstellungen, wie die Dinge zu sein hätten.“
Das hatte er ihm  schon oft gesagt.

Aber als Flügelmann war Sigur eine tolle Sache. 
Mit seinen 14 Jahren hatte er im Spiel schon jede Menge Computer in Banken und Ölfirmen übernommen.
Selbst in  besonders schwer zu knackende Militärcomputer war er schon eingedrungen.
Einmal war es ihm gelungen, ein Passagierflugzeug zu übernehmen, das dann aber abgestürzt war.

Lasse: „Was geht, Sig, hey?“
Sigur reagierte nicht und tippte weiter.
Lasse lugte zu ihm herüber: „Ah! Du bist an der Firewall.
Was ist das Problem?“
Nach einiger Zeit sagte Sigur ohne vom Bildschirm wegzuschauen: „Ich weiß nicht.
Nur so ein Gefühl.
Irgendetwas stimmt nicht.“

<div class="infobox" id="firewall"></div>

Sigur drehte sich zu Lasse um: „Es waren ein paar seltsame Angriffe von einem Yllil-Computer, aber eigentlich nichts Besonders.
Der hat versucht, an ein paar Stellen hineinzukommen, aber die Firewall hat alles geblockt.
Fühlt sich trotzdem komisch an ...“
Lasse: „Yllil? Das klingt Afrikanisch.
– Bei uns stehen heute aber die Chinesen auf dem Plan!“ Er grinste.
„Ich habe super Logdateien von einem Angriff auf einen chinesischen Satelliten gefunden, der fast geklappt hätte.
Da finden wir bestimmt was drin.“

<div class="infobox" id="logfiles"></div>

Lasse: „Auch einen Kakao vorher? Heute holen wir das Ding runter!“

Sigur: „Nein, nicht runter! Übernehmen, alles anschauen, Daten kopieren, Spuren verwischen und wieder abhauen.
Das ist die Mission.“

Lasse: „Ja, ist ja gut – Ich weiß.
Die Mission.
Aber ich würde so gerne wissen, was TRON macht, wenn wir den Satelliten tatsächlich abstürzen lassen würden.
Das würde mindestens eine politische Krise geben, die würden versuchen, das zu vertuschen, die Presse würde alles Mögliche darüber bringen, neue Verschwörungstheorien.
Und dann jede Menge neue Missionen, um uns zu schnappen. He he.“

Sigur schaute ihn streng an und machte eine Handbewegung: „Ruhig!
Wir wollen das Spiel gewinnen, nicht in fünf Minuten rausfliegen.“
