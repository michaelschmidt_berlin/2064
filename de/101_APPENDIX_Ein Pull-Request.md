
<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>
<div class="title-large">Anhang</div>
<div style="visibility: hidden;">\pagebreak</div>

## Ein Pull-Request
<div class="_2064">
    <span class="year">2020</span>
    <hr class="hr" />
</div>

<span class="first-words">Sie (oder er) legte das Buch</span> aus der Hand.

Sie: „Hmmm.
Ich bin durch.
Und was kommt jetzt?“

TRON in dunkler Stimme aus dem Hintergrund: „Ist dir beim Lesen irgendetwas aufgefallen, was du verbessern würdest?“

Sie dachte nach,

TRON: „Vielleicht erst einmal nur einen Rechtschreibfehler?“

Sie lächelte.

Sie: „Bei einer Szene, ich glaube das war das Kapitel über die Sphinx, da hätte ich es stimmiger gefunden, wenn Marlene an einer Stelle gelächelt hätte.“

TRON: „Dann ändere das!“

Sie: „Ok? 
Wie?“

TRON: „So wie Marlene das machen würde, oder Lilly, oder irgendjemand der mit Git arbeitet.“

Sie: „Einfach so?“

TRON: „Du kannst die Änderung direkt im Text vorschlagen. Ich zeige dir wie ...

Gehe mit einem Internet-Browser zu 'https://bitbucket.org 'und logge dich ein.
Wenn du noch kein Konto bei Bitbucket hast, mach dir vorher eines.“

![Pull-Request 1](../../media/tutorials/pullrequest-1.png)

TRON: „Dann klicke auf den 'de'-Ordner für Deutsch.“

![Pull-Request 2](../../media/tutorials/pullrequest-2.png)

Sie: „Und dann suche ich das Sphinx-Kapitel.“

![Pull-Request 3](../../media/tutorials/pullrequest-3.png)

Sie: „Und dann klicke ich Edit rechts oben.
Das ist einfach.“

![Pull-Request 4](../../media/tutorials/pullrequest-4.png)

TRON: „Ja, und jetzt kannst du einfach ändern, was du ändern willst ...“

![Pull-Request 5](../../media/tutorials/pullrequest-5.png)

TRON: „Und dann 'Commit' klicken unten links.
Commit ist eine neue Stufe für das Buch, deine eigene Stufe.“

![Pull-Request 6](../../media/tutorials/pullrequest-6.png)

TRON: „In dem kleineren Fenster musst du nochmal auf 'Commit klicken.'“

![Pull-Request 7](../../media/tutorials/pullrequest-7.png)

TRON: „Und dann passiert jede Menge im Hintergrund. Bitbucket kopiert das komplette 2064-Projekt in dein Konto.
Und dann schickt es einen Pull-Request an den sogenannten Maintainer des Projekts.
Das Ergebnis kannst du dann in einer Übersicht sehen.“

![Pull-Request 8ä](../../media/tutorials/pullrequest-8.png)

Sie: „Ja, da ist meine Änderung.“

TRON: „Und der Maintainer kann jetzt mit einem Klick entscheiden, ob er deine Änderung will oder nicht.
Viel Spaß beim ändern.“

Sie: „Danke!“
