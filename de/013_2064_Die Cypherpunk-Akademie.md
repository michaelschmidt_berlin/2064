## Die Cypherpunk-Akademie
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Edgar sprach eindringlicher:</span> „Marlene Farras war 16 Jahre alt, als sie die NSA-Schwachstellen-Datenbank gehackt hat.
Und dann den Mut hatte, sie allen Menschen zur Verfügung zu stellen.
Es war ein großes Risiko, allen zu ermöglichen, alle Hintertüren zu verwenden, die sonst nur Geheimdienste, große Firmen und Hacker nutzten.
Es war ein Wendepunkt im Befreiungskampf des Internets.
Das hat damals auf der ganzen Welt ein Erdbeben ausgelöst.
Plötzlich konnten alle klar sehen, und am eigenen Leib spüren, wie viele Hintertüren und Schlupflöcher es überall im Internet gab.
Es war regelrecht durchlöchert, jeder konnte auf einmal in jedes Facebook-, Instagram- oder anderes Konto hineinschauen und Nachrichten versenden, oder auch Geld online überweisen oder Spionageprogramme auf Computern installieren.

Das Internet war für die junge Generation damals schon zu einem wirklichen Lebensraum geworden.
Und als sie nachvollziehen konnten, was alles im Geheimen passierte, was getan wurde, um sie zu durchleuchten und zu kategorisieren, und dann zu behindern und einzuschüchtern, änderte sich für sie alles.
Über diese Hintertüren und Schlupflöcher wurden nicht vor allem Verbrecher oder Terroristen gejagt, nein, sie wurden genutzt, um die neue Generation zu überwachen und zu steuern.
Viele von ihnen wurden an diesem Wendepunkt Cypherpunks.

Und das, was Lilly gelungen ist, weswegen wir hier alle sitzen, und uns auch hunderttausende Schüler von überall auf der Welt zuschauen, das ist, diesen berühmten Hack nachzuspielen.
Er war von Anfang an in TRON eingebaut und wartete darauf, dass ihn jemand finden würde.
Lilly hat ihn gefunden und im ersten Anlauf gemeistert.“

Gejohle im Publikum.
Lilly lachte und strahlte über das ganze Gesicht.

Edgar: „Ja ... Und damit geht das TRON-Spiel in eine neue Phase.
Und in der ändert sich einiges.
Das kann ich euch jetzt schon sagen.
Es gibt viel Neues, aber vor allem gibt es eine neue Regel im Spiel.
Und die erklärt euch jetzt Lilly.“
Er winkte zu ihr herüber.

Lilly schaute ins Publikum.
„Hmm.
Ich bin ein bisschen aufgeregt.
So flattrig in der Bauchgegend.
Ja ...
... Wie ist die neue Regel? Sie ist ziemlich einfach.
Sie besagt, dass wir ab jetzt alle Missionen auch als Cypherpunk-Missionen spielen können.
Egal ob ihr Geheimdienstmitarbeiter seid oder Unternehmenschef, Politiker, Hacker, der amerikanische Präsident, egal.
Ihr könnt TRON zum Anfang einer Spielrunde oder auch irgendwann zwischendrin sagen, dass ihr ab jetzt Cypherpunk seid und dann bekommt ihr eure Punkte nicht mehr für Herrschaft, Geld und Kontrolle, sondern für Veröffentlichung von Geheimnissen, für Ungerechtigkeiten ausgleichen und für Bewusstsein schaffen, wie die Welt tatsächlich funktioniert.“

Lilly schaute zu Marlene: „Marlene, willst du vielleicht noch etwas dazu sagen?“

Marlene: „Ja, gerne.
Wie Edgar schon gesagt hat, ging es den Cypherpunks bei Weitem nicht nur um Verschlüsselung, offene, freie Software und verteilte Netzwerke.
Es ging den Cypherpunks um eine Verwandlung der Kultur, des Umgangs der Menschen miteinander.
Dafür war Verschlüsselung ein erster Schritt, weil sie uns ermöglicht hat, im Internet unbeobachtet Dinge zu tun.
Das gleiche gilt für offene und freie Software und verteilte Dienste im Internet.

<div class="infobox" id="encryption-opensource-distribution"></div>

Wir mussten dafür sorgen, dass wir in Ruhe arbeiten konnten, dass wir selbst entscheiden konnten, was wir im Internet machen.
Das war die Grundlage.
Ohne Verschlüsselung hilft alles andere nichts, dann wären wir allen ausgeliefert, die Zugriff auf das Internet haben.
Aber als wir das hatten, dann ging es richtig los.

Wir hatten die Ideale dieser Zeit vor uns: Macht, Geld, Kontrolle.
Das waren die Gottheiten von vielen Menschen damals.
Sie wurden angebetet wie in frühren Zeiten die Götter.
Für viele waren sie das Allerwichtigste im Leben.
An dieser Stelle wollten wir andere Ideale haben.
Zum Beispiel sollte die ganze Welt der Unternehmen und Regierungen transparent werden.
Wir wollten einen Schritt in Richtung einer wirklichen Demokratie gehen und Verbrechen nicht mehr mit Bestrafung und Umerziehung begegnen.
Das sind Dinge, um die es geht, wenn ihr Cypherpunk werden wollt.“

Lilly: „Und wenn ihr das üben wollt, wenn ihr das in der Tiefe kennen lernen wollt, gibt es einen ganz neuen Platz für euch im Spiel.
Und das ist wirklich ein Hammer ...“
Sie zwinkerte zu Marlene herüber.
„Marlene hat das mitprogrammiert.
Ich bin sehr gespannt, was ihr dazu sagt.
Ich habe mich darin schon ein wenig umgeschaut, es ist ziemlich groß, und man kann dort eine Menge machen.
Es ist ... die neue ... Cypherpunk-Akademie!
Eine Akademie, auf der ihr richtige Cypherpunks werden könnt.
Echt!
Und sie ist irgendwo in TRON.
Ihr könnt sie finden.“

Das Publikum johlte auf.

Lilly: „Das ist wie ein Spiel im Spiel, es ist nicht einfach.
Ihr müsst da viel zusammenarbeiten und kreativ werden, denken und schnell reagieren.
Auf jeden Fall geht es um die elf Grundgedanken des Befreiungskampfes, also Verschlüsselung oder besser: alles Unbesiegbare, dann Überwindung der Unterdrückung, Überwindung des Gegeneinanders, Entfaltung der Initiative, ...“
Sie unterbrach und schaute wieder zu Marlene: „Ich habe darüber in deinem Buch einiges gelesen.
Kannst du die vielleicht vorstellen?
Das fände ich super.“

Lasse rief von seinem Platz: „Ja! Das wäre super!“

Marlene nickte: „Das mache ich gerne.“
Sie wandte sich zum Publikum.
„Aber vorher machen wir eine kleine Pause, oder? Das Büffet wartet schon.
Und Essen ist auch wichtig.“
