## Auf einer Berliner Dachterrasse
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Amelie saß mit einem Mikrofon</span> in der Hand auf dem Geländer einer Dachterrasse in Kreuzberg.
Hinter ihr ein wolkenloser Himmel und eine Dächerlandschaft.
Die Abendsonne tauchte alles in warme Farben.
Joe hielt mit der Kamera auf sie.

Amelie: „Willkommen bei Amelies Out of Berlin Spezial! Superspezial.
Mit den Superhelden des 21. Jahrhunderts!
Mit Marwin, gerade eben aus den Klauen des CIA befreit, wo er zwei Jahre gefangen war.
Und Marlene, die ihn dort raus geholt hat!“

Die Kamera drehte zur Terrasse.
Marwin lag neben Marlene auf einem Liegestuhl und winkte in die Kamera.

Amelie: „Supersexy!
Und befreit wurde er von der Superheldin Marlene.“
Marlene lachte und winkte auch.

Gegenüber lagen Oskar und Anni auf zwei Liegestühlen.
Kevin saß auf dem Geländer mit dem Rücken zu den anderen, die Beine nach draußen baumelnd und schaute in Richtung Kottbusser Tor.

Amelie: „Wir lassen jetzt einfach mal die Szene hier sprechen.“

Marwin legte seinen Laptop auf den Boden und zog zwei Flaschen Mate aus dem Eiskübel.
Eine davon gab er Marlene.
Die andere verlangte Oskar.
Er warf sie ihm zu.
Dann holte er zwei weitere und bot Amelie davon eine an, aber sie schüttelte den Kopf.
Er prostete Marlene und Oskar zu, trank einen Schluck, lehnte sich zurück und blickte in die Sonne.
Marlene schaute ihn an, schüttelte den Kopf und lachte los.
Sie lachte und lachte.
Sie konnte nicht mehr aufhören.
Marwin schaute sie an, hielt sich dann mit einer Hand seine Augen zu und lachte mit.
Oskar hielt seine Flasche theatralisch in die Luft und stieß einen Ur-Schrei aus: „Jaaaaaaaahhhhhhh!“

Ein seltsam brummendes Geräusch kam von hinter dem Geländer.
Kevin deutete darauf.
Oskar stand auf und ging mit der Mateflasche zu ihm.
Joe folgte mit der Kamera.
In dem Augenblick stiegen vor ihm zwei, etwa einen Meter breite, ferngesteuerte Drohnen hoch, beide mit einem gläsernen Cockpit oben drauf.
Oskar wich leicht zurück.
Sie standen jetzt fast in Greifweite vor ihm in der Luft.
Perfekt nebeneinander, beide deutlich mit vielen Kameras und anderen Sensoren bestückt.
Sie drehten sich langsam zu Oskar hin.
Auf der Scheibe des Cockpits erschien eine grüne, blinkende Computerschrift, die abwechselnd „Hi!“ und „Oskar“ anzeigte.

Oskar: „What the fuck!“

Er ging näher hin.
Sie flogen genau auf seine Kopfhöhe.
Er schaute sie an, schaute sich ein wenig verwirrt um.
Dann drehten sich beide gemeinsam, exakt synchron, einmal um sich selbst und blieben wieder in Richtung von Oskar stehen.
Zwei lange Wasserstrahlen schossen aus kleinen Röhrchen unter den Drohnen direkt auf Oskars Brust.
Eine rote Laufschrift auf den Cockpitscheiben zeigte: „Kill confirmed.
Kill confirmed.“ und „Oskar down! Oskar down!“ Oskar drehte sich wild suchend um.
Er zog sich an seinem nassen T-Shirt, beugte sich dann über das Geländer und schnappte nach einer der Drohnen.
Die wich elegant zurück und schoss beim Zurückgehen noch einmal einen Wasserstrahl.
Diesmal mitten in Oskars Gesicht.

In dem Augenblick sprang die Tür auf und auf die Dachterrasse kamen lachend zwei etwa 18-jährige Jungs: Ein schmächtiger lateinamerikanisch aussehender und ein schwarzer mit athletischem Körperbau.
Beide hatten kompliziert aussehende Fernsteuerungen in der Hand, zeigten auf Oskar und prusteten los.

Marwin und Marlene schauten ein wenig fragend auf die Szene.
Oskar wischte sich mit dem Ärmel über das Gesicht, zeigte in die Richtung der beiden und sagte trocken zu Marwin und Marlene: „Darf ich vorstellen: Ali Steffens und Eduardo Lampresa“, dann zu den beiden: „Marwin und Marlene Farras.“
Dann ging er zu ihnen und sie umarmten sich lange.
Marwin und Marlene kamen hinzu, und auch Kevin, Anni und Amelie.
