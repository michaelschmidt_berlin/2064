## Die Cypherpunk-Akademie
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Nach einem einstündigen Flug</span> war das Flugzeug auf einem regionalen Flughafen gelandet.
Sigur und Tim stiegen aus.
Neben dem Rollfeld schwebte der riesige, weiße Ballon in etwa 20 Metern Höhe über dem Boden.
Er hüllte die ganze Szene in Schatten.
Seine Außenhaut schien aus härterem Material zu bestehen als normale Ballons, vielleicht aus hartem<x> Plastik oder sogar Metall.
Er sah schwer aus und machte nicht den Eindruck, als könnte er fliegen.
Er hatte auch keinen eigentlichen Korb an der Unterseite, nur eine Art von Gewicht.
Sigur staunte wieder über seine schiere Größe.
Er entdeckte neben dem Gewicht einen Schacht, der in den Ballon hinein zu führen schien und aus dem zwei blaue Seile bis zum Boden herunter hingen.
Darunter wartete Lasse mit einem breiten Grinsen.

Sigur lief los und fiel Lasse in die Arme.

Lasse: „Hej!
So gut dich zu sehen!
Du Cyber-Terrorist!“

Sigur: „Selber!“
Sie lachten und klatschen sich ab.
Tim kam hinzu und Lasse umarmte ihn.

Sigur: „Das war eine Aktion!
Unfassbar!
So viel und so oft habe ich noch nie geschwitzt bei irgendeiner Mission.
Ich habe nicht mehr erwartet, das wir uns in dem Spiel noch einmal begegnen.
Und die Fahrt hierüber, mit Tim, der volle Wahnsinn ...“

Lasse atmete aus.
„Und dann treffen wir uns auch noch hier.“
Er zeigte auf den Ballon.
„Du siehst den Ballon, nicht?“

Sigur: „Ja, klar.
Ist ja groß genug.
Wie soll ich ihn übersehen?
Was ist das eigentlich?
Tim hat ihn vorhin nicht gesehen ...“

Lasse wandte sich zu Tim und zeigte auf den Ballon: „Tim, siehst du den Ballon?“

Tim: „Welchen Ballon schon wieder?
Da ist ein Flughafen, ein Rollfeld, ein paar Wiesen dahinter.
Fängst du jetzt auch schon an?
Was soll die Frage?“

Sigur erstaunt.
„Aber siehst du nicht, wir stehen im Schatten?
Wo kommt denn der her?“

Tim: „Hej, jetzt hör auf!
Hier ist kein Schatten.
Mich blendet die Sonne.“
Und tatsächlich kniff er ein wenig die Augen zu.

Sigur schaute erstaunt zu Lasse: „Was ist das?
Ist er blind?“

Lasse schüttelte den Kopf: „Nein, überhaupt nicht.
Der Ballon ist die Cypherpunk-Akademie.
Die kannst du nur sehen, wenn du Cypherpunk bist.“

Sigur: „_Das_ ist die Cypherpunk-Akademie?
Geil!
Aber ich bin kein ...“

Lasse: „Doch!
Bist du.
Du kannst das aber auch anders nennen, wenn du willst.“
Er schlug ihm stolz auf die Schulter: „Gratuliere.
Komm, schnapp' dir ein Seil.
Wir müssen zur Sphinx.“

Sigur schüttelte den Kopf und nahm eines der Seile: „Und wenn ich kein Cypherpunk sein will...“

Lasse: „Doch, das willst du.
Vertrau mir.
Mach den Fuß so unten in die Schlaufe.
Und dann fass hier an.“

Sigur: „Erst noch von Tim verabschieden.“

Lasse: „Brauchst du nicht!
Das macht TRON.
Alles, was wir mit den Seilen machen, sieht Tim schon gar nicht mehr.
TRON macht, dass wir uns ganz normal verabschieden, oder irgendetwas anderes.
Ist wahrscheinlich schon passiert.
Du kannst dir das dann später anschauen.“

Sigur schaute ihn erstaunt an.
Lasse zwinkerte ihm zu, steckte seinen Fuß in das Seil und sauste nach oben.
Nach ein paar Sekunden war er im Schacht des Ballons verschwunden.
Sigur schaute noch einmal zu Tim, der sich aber schon abgewandt hatte und wieder Richtung Flugzeug ging.
Er war im Gespräch mit irgendjemand Unsichtbaren.

Sigur setzte vorsichtig seinen Fuß in die Schlaufe, schaute nach oben und hielt sich fest.
Dann machte das Seil von selbst einen Ruck und er sauste nach oben.
Seine Hände waren wie festgeklebt, seine Beine wurden von irgendeiner seltsamen Kraft durchgedrückt.
Er wurde immer schneller.
Im Schacht war es stockfinster.
Er sauste weiter nach oben.
Nach einiger Zeit sah er an der Schachtwand um sich herum einzelne Farbpunkte erscheinen, dann kamen sie in Bewegung, wurden mehr und größer und schließlich verwandelte sich alles in ein Meer von bunten Blättern, kleinen Fischen und anderen Dingen, die durcheinander schwebten und flossen.
Er schoss aus der Röhre.
Das Seil verschwand aus seiner Hand und er landete auf einer Plattform in der Mitte einer riesigen Kuppel.
Die Plattform hatte etwa fünf Meter Durchmesser und fiel nach allen Seiten senkrecht nach unten.
Er sah den Boden nicht.
Rechts neben ihm stand Lasse auf einem ähnlichen Plateau.
Sigur hob seinen Daumen zu ihm hin, aber Lasse sah nur kurz zurück und antwortete nicht.
Lasse schaute konzentriert zu einer Stelle weit vor ihm an der Kuppelwand, als ob dort jeden Moment etwas herauskommen würde.
Er ging dabei leicht in die Knie und breitete die Arme aus.

„Willkommen in der Akademie, Sigur“, sagte eine Stimme neben ihm und er drehte sich zu ihr.
Mostafa, die Mosquito-Fliege, flatterte vor sein Gesicht und schaute ihn freundlich an.
„Ich bin Mostafa!
Wir kennen uns schon von der Einführungsveranstaltung.“

Sigur nickte.

Mostafa: „Lasse wartet schon auf die Sphinx.
Das ist immer gefährlich.
Bei der Sphinx weiß man nie, was passiert.
Da muss man wirklich aufpassen.“

Sie schauten eine Weile zu Lasse.

Mostafa: „Du bist der, der mit Lasse die erste LBI-Mission gemacht hat, oder?“

Sigur: „Genau! Und dich kann man alles über die Cypherpunk-Akademie fragen, oder?“

Mostafa: „Nicht nur das.
Ich bin auch der Lehrer für Lesen und Schreiben.
Natürlich das Lesen und Schreiben von Code, von Programmen.
Cypherpunks schreiben Programme.
Das ist quasi das Einmal-Eins hier.
Dafür haben wir den Cypherpunk-Computerspielplatz.“

Sigur: „Ich kann programmieren.“

Mostafa: „Ich weiß.
Deswegen gehen wir nicht gleich auf den Spielplatz.
Aber es gibt dort schon noch einiges, was du nicht kennst.
Das kommt für dich dann später dran.“

Sigur lugte wieder herüber zu Lasse, der seltsame ausweichende Bewegungen machte: „Muss ich auch zur Sphinx?“

Mostafa: „Du musst nicht.
Hier in der Akademie muss man nie etwas.“
Er grinste.
„Du musst schon, wenn du zu den interessanten Plätzen kommen willst.
Die Sphinx bewacht alle Aus- und Eingänge in der Akademie.
Du musst immer wieder an ihr vorbei, sonst kommst du nicht einmal in die Akademie hinein.“

Sigur: „Wie?
Das hier ist noch nicht drin?“

Er zeigte auf die riesige Kuppelhalle, die in seinen Augen ungefähr so groß sein musste wie der ganze Ballon.
Er schaute nach rechts und erschrak.
Lasse war plötzlich verschwunden und dort, wo vorher seine Plattform war, stand jetzt ein riesiger Felsblock mit einem überdimensionalen Bienenkorb in der Mitte.
Oben drauf saß Wim, der Bär, und blickte mit ruhigen Augen zu ihnen herüber.

Sigur staunte: „Was ...?
Wo ist ...?“

„In einer Parallelwelt.
Die Akademie besteht aus vielen Parallelwelten.
Komm mit“, meinte Mostafa und flog los.
„Wim wartet auf dich.“

„Wie soll ich da hinüber kommen?
Ich kann nicht fliegen und zum Springen ist es zu weit“, rief Sigur hinter ihm her und schaute die Kante der Plattform hinunter.

Mostafa rief von der anderen Seite: „In der Cypherpunk-Akademie geht alles mit Denken.
Denk dich dorthin!“

Sigur nickte Mostafa zu: „Ich verstehe.“
Er trat einen Schritt zurück, legte seine Hände seitlich an die Stirn und sagte leise: „Zum Bär, zum Bär.“
Nichts rührte sich.

Mostafa: „Worte sind nicht Denken.
Du musst loslassen.
Du musst richtig denken.
Vertrau!“

Sigur probierte es noch ein paar Mal und wiederum tat sich nichts.

Mostafa flog wieder zu ihm herüber: „Na?
Fehlzündung?
Du musst nicht denken, dass du dahin willst, sondern denken, dass du dort bist.“

Sigurs Ohren ploppten, es ging ein Stich durch seine Augen, er kniff sie zu, und als er die Augen wieder öffnete stand er direkt vor dem Felsen des Bären.
Ihm wurde schwindlig.
Er stolperte nach rechts und konnte sich gerade so fangen.
Mostafa flog unter seinen Arm und stütze ihn.

Mostafa: „Kein Problem, kein Problem.
Das ist immer beim ersten Teleport.
Gleich wird dir schlecht werden.
Dein Gehirn registriert ziemlich langsam, dass du jetzt woanders bist, ohne dahin gegangen zu sein.
Das kennt es noch nicht.“

Sigur fasste sich an den Bauch.
Er kniete sich hin.

Wim sprang vom Bienenstock herunter und setzte sich im Schneidersitz vor Sigur auf den Boden.
Der richtete sich nach einer Weile auf und setzte sich auch.
Von Wim ging eine angenehme Atmosphäre aus.
Er schaute Sigur mit wachen, interessierten Augen und einem unsichtbaren Lächeln an.
Sigurs Körper entspannte sich.
Er fühlte sich leichter.
Die Anstrengungen der letzten Tage fielen von ihm ab.

„Willkommen in der Akademie, Sigur“, brummte der Bär mit einer dunklen Stimme.
„Dieser Stein markiert den Eingang zu den Ebenen-Inseln.
Du wirst sehen, warum sie so heißen.
Es gibt elf davon.“

Sigur: „Elf? So viele wie Grundgedanken?“

Wim: „Genau.
Und über der elften Ebene schwebt mein Garten.
Der Garten des Epikur.
Das ist das Ziel.
Es wird eine Weile dauern, bis du dahin kommst.
Die Aufgabe ist, den Weg dorthin zu finden.

Auf dem Weg über die 11 Ebenen findest du viele stark befestigte Städte, mit jeweils einem Tempel im Zentrum.
Du spielst einen Ritter im frühen Mittelalter.
Die Tempel sind die Hüter der Wege, sie wachen darüber, wer welchen Weg zur nächsten Stadt, oder zu einem Wald oder einem Fluss gehen kann.
Sie haben alle ein Rätsel, jeder seins, das ist ein kultureller Gedankenfehler, den man auf unterschiedliche Weise lösen kann.
Ein Gedankenfehler, der dich daran hindert, Cypherpunk zu werden.
Den musst du lösen.
Und nicht nur auf eine Weise, sondern auf mindestens vier.
Du kannst später auch eigene Lösungen hinzufügen, mit einem Pull-Request, und sie können Teil eines Tempelrätsels werden.
Dadurch kannst du Anwärter, Helfer und am Ende Diener eines Tempels werden.
Tempeldiener ist das höchste, was du erreichen kannst.
Es gibt ja keine Tempel-Chefs, oder Priester oder so etwas.

<div class="infobox" id="pullrequest"></div>

Wenn du noch keine Stadtrechte hast, musst du eine Stadt zunächst erobern, mit damaligen Waffen und nicht alleine.
Das geht nicht.
Es gibt immer wieder Angriffe von vorüberziehenden Armeen, denen du dich anschließen kannst.
Und du brauchst Fähigkeiten, dass du von einer Armee aufgenommen wirst.
Solche Fähigkeiten bekommst du vor allem, wenn du Programmier- und Hackaufgaben löst.
Wenn du auf der 11. Ebene ankommst, wirst sehr gut programmieren können.
Und hacken, dass es dem Kommando Center Taurus ganz Angst und bange wird.“

Sigur: „Ich kann programmieren und hacken.“

Wim schmunzelte: „Ja und nein.
Du wirst sehen.“

Sigur schaute Wim skeptisch an.

Wim: „Jetzt wirst du den nächsten Lehrer der Akademie treffen.
Er wartet am Eingang zu den Ebeneninseln.
Das ist hier!“

Wim zeigte neben sich.

Sigur nickte. Er schaute an die Stelle, wohin Wim zeigte.

Sigur: „Wo?“

Wim: „Bist du bereit?“

Sigur: „Hinein zu gehen?
Ja!“

Wim: „Bist du bereit für das, was kommt.“

Sigur: „Was kommt denn?“

Wim: „Das, was kommt, das kommt.
Schau dich um!
Ist irgendetwas Gefährliches um dich herum?
Bedroht etwas dein Leben.“

Sigur schaute sich um, schüttelte den Kopf.

Wim: „Wenn sich irgendetwas unsicher anfühlt, fokussiere darauf.“

Sigur schloss die Augen.
Öffnete sie wieder: „Nichts.“

Wim: „Frage dich: Hast du alles, was du brauchst.
Gibt es etwas, auf das du wartest, das nötig ist, bevor du weiter machen kannst?
Muss du etwas wissen? Oder klären?
Oder kannst du einfach hier sein, so wie du bist?“

Sigur schloss seine Augen wieder und atmete ruhig.
Er nickte.

Sigur: „Was kommt jetzt?“

Aber Wim war schon verschwunden, der Bienenkorb auch, auch Sigurs Plattform war weg.
Die ganze Kuppel änderte sich in atemberaubender Geschwindigkeit.
Sigur stand instinktiv auf und machte einen Schritt zurück.
Er schaute nach allen Seiten.
Er stand jetzt auf einer Felsen-Ebene, die nach allen Seiten bis zum Rand der Kuppel reichte.
Alles wurde dunkler, bekam schärfere Kanten und Umrisse und plötzlich hörte man in der ganzen Kuppel ein düsteres Grollen.
Der ganze Raum erzitterte, dann stand auf einmal ein riesiger, kreisförmiger Bereich der Kuppel in Flammen.
Sigur starrte dorthin.
Manche Flammen schlugen weit in die Kuppel hinein.
Urplötzlich schoss aus der Mitte des Kreises mit hoher Geschwindigkeit eine dunkle Gestalt mit Vogelkopf und gewaltigen Schwingen heraus und flog nach oben zum Kuppeldach, dann von dort direkt auf Sigur zu.
Sie ließ etwa ein Dutzend eiförmige, brennende Gebilde Richtung Sigur fliegen.
Er warf sich auf den Boden, drehte sich zur Seite und konnte gerade so vermeiden, getroffen zu werden.

Sigur: „Scheiße, die Sphinx!“

Die Sphinx landete etwa zwanzig Meter vor ihm auf dem Boden und wurde größer und größer und größer, bis sie etwa ein Drittel der Höhe der Kuppel einnahm.

Sigur sagte leise: „Die Sphinx!“

Sie beugte sich zu ihm herunter und fragte mit lauter, grollender Stimme:

„WER BIST DU, DASS DU ES WAGST, VOR MICH ZU TRETEN?“

Sigur richtete sich auf und antwortete, ohne zu zögern: „Ein Mensch!“
Er wusste, dass er schnell antworten musste.
Eine Drittelsekunde etwa, um die Sphinx zu schlagen.
Das war seine drei Sekunden-Frage gewesen ... weg.

Die Sphinx lies ihre Flügel hochflattern und schrie in Schmerzen auf.
Dann sammelte sie ihre Kräfte und legte die Flügel an.
Sie war ein sichtbares Stück geschrumpft und schaute Sigur dunkler und strenger an.

Sphinx: „WAS WILLST DU HINTER DIESER PFORTE TUN?“

Sigur: „Lernen.
Ich will lernen.“

Die Sphinx schrie zum zweiten Mal markerschütternd auf.
Sigur schaute sie hochkonzentriert an.
Immer wenn er „richtig“ antworten würde, würde sie an Kraft verlieren, und wütender werden.
„Richtig“ hieß, aus ihrer Sicht richtig.
Das war wichtig, das hatte ihm Lasse gesagt.
Die Sphinx wähnte sich im Besitz der ganzen Weisheit der Welt, der vollständigen Wahrheit.
Um die Sphinx zu besiegen, musste man verstehen, wie sie dachte.

Die Sphinx beruhigte sich wieder, beugte sich zu Sigur und drehte dabei drohend ihren Kopf hin und her:

Sphinx: „WELCHE TUGEND BRAUCHST DU HIER AM MEISTEN?“

Sigur: „Gerechtigkeitsliebe“

„Das war einfach“, dachte Sigur, „Die Feinde des Internets im Jahr 2028 waren darauf aus, die Welt zu spalten, in Menschen, die in Wohlstand lebten und solchen, die für sie arbeiten sollten.“

Der Schrei der Sphinx wurde immer mehr zu einem Aufjaulen.
Ihre Augen wurden düsterer, sie begann deutlich an Kraft zu verlieren, ihre Flügel wurden steifer, sie schrumpfte weiter.

Sphinx: „WAS IST STÄRKER ALS ALLE GEHEIMDIENSTE UND ARMEEN UND UNTERNEHMEN DIESER WELT?“

Sigur stutzte einen kurzen Moment.
Es hatte es einmal gewusst.
Es war ihm entfallen.
Aber es war so einfach gewesen.
Was war das?
Er konzentrierte sich.
Die Sphinx begann sich aufzuplustern.

Sigur: „Die Jugend!“ Er atmete aus.
Das war knapp.
Man musste innerhalb von drei Sekunden antworten, sonst war es zu spät.

Die Sphinx rauschte mit ihren Flügeln.

Sphinx: „WAS TUST DU, WENN DU VERSAGST?“

Sigur: „Ich kann nicht versagen.
Wenn ich Fehler mache, lerne ich, wenn ich etwas richtig mache, freue ich mich.“

Die Sphinx seufzte, laut und kläglich.
Ihr Stimme wurde brüchig.
Sie zitterte leicht.

Sphinx: „DU WIRST DICH HIER VERLIEREN!“

Sigur nickte deutlich.

„Und genau das will ich!“, sagte er mit einem breiten Grinsen.

Eine dunkle, warme Stimme erklang aus dem Hintergrund:

TRON: „Sage es noch einmal anders.“

Sigur ernster: „Ich will mich verlieren, um mich zu finden.“

Ein klägliches Krächzen kam aus dem Schnabel der Sphinx.
Sie kauerte auf dem Boden und wandte sich vor Schmerzen.
Dann richtete sie sich mit letzter Kraft wieder auf.

Sphinx: „DU KANNST GEGEN MICH NICHT GEWINNEN.“

Das war eine Trickfrage.
Das Wort „gegen“ war das Entscheidende.
Sigur sagte triumphierend: „Aber ich kann gewinnen. Immer wieder!“

Er sprang von seinem Podest, lief auf die Sphinx zu, die zu Stein erstarrt war, und gab ihr einen Tritt.
Alles zerbröselte zu Staub und wurde nach und nach von einem aufkommenden Wind herausgetragen.

Die ganze Halle änderte sich wieder.
Sigur sah um sich herum eine mittelalterliche Stadt entstehen.
Er stand auf dem Hauptplatz in der Mitte der Stadt, vor ihm ein großer Tempel aus fein bearbeitetem roten Sandstein und Marmor.
Überall liefen Menschen vorbei, die irgendeiner Aufgabe nachgingen.
An ihrer Kleidung konnte man erahnen, was sie wohl taten.
Am Rand des Platzes konnte man eine Reihe Häuser und Geschäfte sehen. 
Rechts, etwas weiter weg einen Marktplatz und weiter hinten ein Stadttor.

Von hinten sagte eine klare, weiche Stimme: „Sigur!“ Er drehte sich um und vor ihm saß auf einer Holzbank, Tin, die Eule.

Sigur verneigte sich.

Tin: „Auch ich heiße dich herzlich Willkommen.“

Sigur: „Ist das so ein Tempel, von dem Wim gesprochen hat?
Warum bin ich jetzt schon hier?
Ich wollte zum Eingang, Städte muss man erobern.“

Tin: „Du bist im Übungsraum.
Das hier ist eine Stadt, in der du Tempeldiener bist, das höchste, was man in einer Stadt werden kann.
Hier kannst du herkommen und schauen, was du alles erreichen kannst.
Aber es ist alles auch ein wenig versteckt.
Du musst auch herausfinden, was es alles gibt.
Aber es ist alles da, was es dann im Spiel gibt.
Da vorne gibt es Waffen, Kleidung, andere Ausrüstung, Landkarten.
Jeder, der hier vorbeiläuft weiß irgendetwas oder kann irgendetwas.
Auf dem Markt bekommst du Heilmittel und Edelsteine, besondere Metalle.
Du wirst verstehen, wann du sie brauchst.
Aber das Wichtigste ist das hier.“

Tin nickte in Richtung des Tempels.

Sigur nickte auch.
Sigur: „Klar.
Was kann ich hier machen?“

Tin: „Als Tempeldiener musst du nicht in den Tempel hineingehen, um alles mit ihm machen zu können.
Du kannst von überall aus, auch in anderen Städten ein Fenster zu dem Tempel öffnen.
Denk an den Tempel und streiche mit der Hand so durch die Luft.“

Tin malte mit der Hand einen Halbkreis in die Luft.
Sigur ahmte die Bewegung nach.

Ein etwa drei Meter breiter und 1,5 Meter hoher Bilderrahmen erschien vor ihnen in etwa ein Meter Höhe.
Er zeigte den Satz:

<div class="terminal">
Man muss sich anpassen, wenn man im Leben etwas erreichen will, wenn man erfolgreich sein will.
Aber das, auf was man deswegen verzichten muss, weil man sich anpasst, das kann man durch anderes ausgleichen.
Zum Beispiel dadurch, viel Geld zu haben, das schönste Haus, den interessantesten Partner, die aufregendsten Reisen zu machen oder irgendetwas anderes Spannendes oder Schönes.
</div>

Zur gleichen Zeit leuchtete der gleiche Satz an der Vorderseite des Tempels auf.

Sigur lächelte: „Oder den besten Computer ...
Aber etwas stimmt da schon dran.
Man muss sich ein bisschen anpassen, oder nicht?“

Tin schaute ihn fragend an.

Sigur: „Ein bisschen.
Man kann nicht alles so machen wie man will.“

Tin: „Man muss sich nie anpassen.
Es kommt drauf an, ob du es schaffst, etwas zu wollen, was du vorher vielleicht nicht wolltest.
Anpassen musst du dich nur, wenn du etwas tust, was du nicht tun willst.
Das ist dann ein Verrat an dir selbst.
Es gibt viele Menschen, die das nicht einmal können.

Im wirklichen Spiel ist das, was du als Tempel-Diener am Tempel liest, dein eigener Satz.
Der steht da so, wie du ihn selbst formuliert hast und wie ihn andere verstanden und verwendet haben.
Und wenn du, wie du gerade, einen Gedankenfehler in einem Tempel nicht für richtig hältst, dann kannst du Herausforderer werden.
Dann legst du so etwas wie deinen Kommentar mit dem ein bisschen anpassen auf den Altar.
Und dann können Tempeldiener und andere darauf antworten und vielleicht den Gedanken anpassen.

Im Jahr 2028 haben viele Menschen tatsächlich so gelebt als wäre der Satz richtig.
Sie wollten viel Geld haben, das schönste Haus und so weiter.
Und sie haben auf viel verzichtet, um erfolgreich zu sein.“

Sigur nickte.

Tin: „Alles ist schief, wenn man es nur aus seiner eigenen Perspektive betrachtet.
Auf der anderen Seite ist alles tot, was man nur mit fremden Worten versteht.
Deswegen wirst du hier in der Akademie deine eigenen Worte, deine eigene Beschreibung für die Gedankenfehler finden.
Es reicht nicht aus, dass du wiederholst, was ein anderer gesagt hat.
Du wirst üben, alles von verschiedenen Seiten aus zu sehen und zu Gedanken zu kommen, die lebendig sind, statt nur Worte zu haben.
Es gibt nicht die Wahrheit, es gibt nur Wahres.
Die widersprüchlichsten Dinge können wahr sein.“

Tin musterte Sigur.

Tin: „Ich sehe, du willst jetzt schnell weitergehen ...
Aber du bist noch nicht bereit.
... du musst auch noch an den Ebenen-Sphinxen vorbei.“

Sigur: „Ich weiß, Tin.
Die Ebenen-Sphinxen.
Ich bin bereit.“

Tin: „Du bist bereit?“

Sigur: „Ich bin bereit!“
Er stellte sich aufrecht vor sie.

Tin musterte ihn mit besorgten Augen.

Tin: „Auf welche Ebene willst du?“

Sigur: „Auf welcher Ebene ist Lasse?“

Tin: „Auf Ebene 4.“

Sigur: „Dann auf Ebene 4!“

Tin nickte: „So soll es sein.
Ebene 1: Freie Software, Verschlüsselung, dezentrale Dienste und alles andere, was jeder Macht der Welt standhalten kann.“

Sigur nickte zurück: „Ich kenne die 11 Grundgedanken der Cypherpunks.“

Tin: „Ich weiß, ich weiß.
Aber kennen reicht nicht, du musst verstehen.
Ebene 1 wird dir geschenkt.
Du kannst jederzeit dorthin gehen und die Städte aufsuchen.“
Sie warf einen bunt leuchtenden Glasball in die Luft, der fortschwebte und nach einer Zeit in Tausende von kleinen Stäubchen in allen Farben zerfiel.
Sigur bestaunte sie lachend.

Sie sah Sigur an: „Ebene 2: Eigene Ideale: Gerechtigkeit, Offenheit, Gleichheit, Brüderlichkeit, statt Geld, Macht und Sicherheit.“

Sigur nickte: „Freiheit, andere Anteil haben lassen ...“

Tin: „Ebene 2 ... wird dir ebenfalls geschenkt.
Das ist sehr selten, dass zwei Ebenen geschenkt werden. Du kannst sie natürlich jederzeit besuchen und erforschen.“
Sie warf eine andersfarbige Kugel in die Luft, die auch zerstob.

Sigur lächelte.
Seine Augen strahlten.

Tin: „Ebene 3: Das Dagegen-Sein überwinden, und alles andere, was die Mauern zwischen Menschen überwindet.“

Sigur nickte.

Tin: „Ebene 3: Die Ebenen-Sphinx wartet auf dich.
Bist du bereit?“ Sie schaute ihn eindringlich an.

Sigur: „Ja.“

Tin flatterte mit ihren Flügeln und in einem Blitz verschwand sie und der Tempel mit dem Hauptplatz und der ganzen Stadt und Sigur fand sich in einem Kerker ohne Fenster wieder.
Überall grob gehauene, feuchte Steine, von Mörtel zusammengehalten, viel Moos und ein strohbedeckter Steinboden.
Es roch scharf nach Fäkalien.
Sigur hielt sich die Nase zu.
Er schaute sich verwundert um.
Das kannte er nicht.
Davon hatte ihm Lasse nicht erzählt.
Er dachte nach, lief herum, schaute die Wände an.
Das einzige Licht kam von einer winzigen Gittertür mit massiven Eisenstäben an einem Ende des Raumes.
Sie war gerade so groß, dass er durch sie herauskriechen könnte, wenn sie denn offen wäre.
Er prüfte die massiven Scharniere, fand aber keine Schwachstelle, sogar das Schloss fehlte.
Das Gitter war festgeschweißt.
Er umfasste die Stäbe, rüttelte daran.
Unmöglich sie auch nur einen Millimeter zu bewegen.
Er rief laut den daran anschließenden Gang hinunter.
Keine Antwort.
Er versuchte sich ins Cypherpunk-Café zu denken, zurück zum Bären, in die Übungsstadt, aber nichts geschah.
Dann fasste er sich mit beiden Händen an den Kopf: „Oh nein!
Nicht Wunderlich, nicht Wunderlich.“

„Doch!“, sagte eine Stimme hinter ihm.
Sigur drehte sich um.
Eine kleine Ratte blickte ihn mit großen, scheuen Augen an.

Er hielt beide Hände vor sein Gesicht und dachte: „Oh, nein!
Ich bin kaum zehn Minuten hier und schon bei Wunderlich.
Lasse war in seiner ganzen Zeit erst zweimal bei ihm!“

Wunderlich: „Ja, ja.
Du hast großen Mist gebaut, du Cypherpunk!“
