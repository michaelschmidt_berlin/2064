## On a Berlin roof terrace
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Amelie sat with a microphone</span> in her hand on the railing of a roof terrace in Kreuzberg.
Behind her a cloudless sky and a rooftop landscape.
The evening sun bathed everything in warm colours.
Joe pointed the camera at her.

Amelie: "Welcome to Amelies Out of Berlin Special! Super special.
With the superheroes of the 21st century!
With Marwin, just freed from the clutches of the CIA where he was held for two years.
And Marlene, who got him out!"

The camera turned to the terrace.
Marwin lay next to Marlene on a deck chair and waved into the camera.

Amelie: "Supersexy!
And he was freed by the superheroine Marlene."
Marlene laughed and waved too.

Oskar and Anni were lying on two deckchairs opposite.
Kevin sat on the railing with his back to the others, his legs dangling outside and looking towards the Kottbusser Tor.

Amelie: "Let's just let the scene speak for itself."

Marwin put his laptop on the floor and pulled two bottles of mate out of the ice bucket.
He gave one of them to Marlene.
The other asked for Oskar.
He threw it to him.
Then he fetched two more and offered one of them to Amelie, but she shook her head.
He cheered Marlene and Oskar, took a sip, sat back and looked into the sun.
Marlene looked at him, shook her head and started laughing.
She laughed and laughed.
She couldn't stop.
Marwin looked at her, then covered his eyes with one hand and laughed along.
Oskar held his bottle theatrically in the air and let out a primal scream: "Yaaaaaahhhhhhh!"

A strange humming sound came from behind the railing.
Kevin hinted at it.
Oskar got up and went to him with the bottle of maté.
Joe followed with the camera.
At that moment, two remote-controlled drones, about one meter wide, both with a glass cockpit on top, rose up in front of him.
Oskar retreated slightly.
They were now almost within reach in front of him in the air.
Perfectly side by side, both clearly equipped with many cameras and other sensors.
They turned slowly towards Oskar.
A green, flashing computer font appeared on the cockpit windshield, alternately displaying "Hi!" and "Oskar".

Oskar: "What the fuck!"

He went closer.
They flew right up to his head level.
He looked at her, looked around a little confused.
Then both turned together, exactly synchronously, once around themselves and stopped again in the direction of Oskar.
Two long jets of water shot from small tubes under the drones directly onto Oskar's chest.
A red ticker on the cockpit windows showed: "Kill confirmed.
Kill confirmed." and "Oscar down! Oscar down!" Oskar turned wildly searching.
He pulled on his wet T-shirt, then bent over the railing and snapped at one of the drones.
She retreated elegantly and shot a water jet again on her way back.
This time in the middle of Oskar's face.

At that moment the door opened and two boys about 18 years old came laughing onto the roof terrace: one slim Latin-American looking and one black one with an athletic build.
Both had complicated looking remote controls in their hands, pointed at Oskar and snorted away.

Marwin and Marlene looked at the scene a little questioningly.
Oskar wiped his sleeve over his face, pointed in the direction of the two and said dryly to Marwin and Marlene: "May I introduce you: Ali Steffens and Eduardo Lampresa", then to the two: "Marwin and Marlene Farras."
Then he went to them and they embraced for a long time.
Marwin and Marlene joined them, as well as Kevin, Anni and Amelie.
