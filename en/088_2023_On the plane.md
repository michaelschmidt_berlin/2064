## On the plane
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Marlene sat in a wide armchair</span>, opposite Linda.
Both tried to have a halfway friendly conversation.
Linda asked Marlene about her childhood.
On the other side Amelie spoke into a microphone.
In front of her, Joe on camera.
Through the window you could see a carpet of clouds and bright sunshine.
Further behind them, the president gave an interview to a television crew at a discussion table.

Robert came by and calmly said to Linda: "She wants to talk to you.
In her cabin."

Linda nodded, stood up and disappeared at the end of the cabin behind a door.
The President followed her.

Inside, they both looked at each other for a long time.

Linda: "I know, I know.
It's a risk.
But it'll work.
Actually, everything fits very well into our plan right now.
Just like the pardon thing.
We had them on the schedule anyway."

President: "But... But how could it happen that everything was suddenly so airy, so unplanned, completely uncertain?
How could the Farras suddenly appear there?
Why didn't we know?
Why would they do that?
I mean, they didn't say a word before, or do I not know something?
I don't want anything like this to happen again."

Linda: "They didn't say a word.
That won't happen again.
But our plan will work.
The warden of Virginia Prison is a colonel in the Rangers.
He was already scheduled for the LBI anyway.
So is the NSA director.
They are scheduled for the same area.
That is quite a coincidence.
But we can use that.

Officially, they're both losing their CIA jobs and everyone will take it from us.
Then they get their camouflage in the arms industry.
Two different companies, I think one was Boeing.
No one will be surprised that this happens.
They will both like their new job at the LBI, you can bet on that.
They get more powers and are less controlled.
Everything is finally really secret again."

President: "That sounds good.
I still do not like the risk in this case, these ad hoc decisions, in twenty minutes.
The last time I was on one of those European talk shows.
The LBI is too important to risk so much.
We're gonna need it more and more.
The NSA is burned.
The public knows too much about her.
We need something that is truly all ours again.
Where we can operate freely.
Where not millions of people look at every little movement.
And we spend most of our time keeping the important things under wraps."

Linda: "The good thing is that the thing with Marlene Farras will make the public in Europe understand the many changes in the NSA and the CIA.
This gives us the freedom to throw a large number of people out now and thus free them for the LBI.
Surely there are also some foreign secret services biting and poaching people we officially drop.
Then we get LBI in the FSB or LBI in Mossad or better, the Chinese..."

President: "Yes.
Maybe.
But we can't afford to make any mistakes now."

Linda: "Don't worry.
Marlene is flying back to Berlin with her brother tomorrow.
The two become icons for the transformation of CIA and NSA to the positive.
You'll see them.
This gives people hope.
And that gives us leeway."
