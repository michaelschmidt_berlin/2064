## One albatross
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">In front of a mountain hut</span> there were two off-road motorcycles.
Roger, a tanned forty-something with a beard, was sitting on a bench leaning against the wall of a house, his hands folded behind his head, a cigarette in his mouth, his legs stretched out.
In the house you could see Lasse on his laptop.
He wore headphones and had a large joystick in his hand.
In front of him on the screen was a control program for a flying combat robot.
It was active.
The combat robot flew over a steppe landscape.
In front of him another fighting robot came into view.

<div class="terminal">
Lasse: "Sigur?
I'm here.
Behind you on the right."
<br /><br />
Sigur: "Cool!
The team's back together again.
Feels good.
You know, the last thing we did together like this was take the five star from the sky.
The last loop, you and me together, remember?"
<br /><br />
Lasse: "Sure, I remember that.
And I like to come and bail you out when you're in trouble.
Where are the helicopters?"
<br /><br />
Sigur: "I've already done that.
They're somewhere in the desert sand, maybe 10 miles west of us.
It was quite simple.
The XJ-11 really has a lot more fighting power than its predecessors, and is super agile.
I never felt in danger at any time.
That's not why I waited for you.
I got all three of them down within 30 seconds.
It was really easy.
Of course, I don't have a Hellfire missile now."
<br /><br />
Lasse: "SHIT!
Sigur!
We wanted to chase them away and then disappear.
Don't take it right out of the sky.
There are people in there.
You can't just shoot at anything that gets in your way.
He makes rules.
The following applies: Only to avert immediate danger to life.
Or better still, the cypher punk rule: "Only if the other person directly threatens a life, and then only her, not a few people around her.
<br /><br />
Sigur: "I am not a Cypherpunk.
And yet: Isn't that an immediate danger?
Three Apache helicopters, with heat-guided guided missiles and 30mm cannons.
These are not toys.
I was in danger."
<br /><br />
Lasse: "They have no chance against you.
There are people in there, they have to watch their lives too.
Worst case scenario, you lose a fighting bot."
<br /><br />
Sigur: "Hey, these are people who work for a totalitarian regime, they have to expect that something will come back as an answer, with everything they do.
I have a prussic acid capsule in my stomach.
They would activate them now without batting an eyelid if they could.
They definitely do worse things than me.
And you have this thing in your brain that you don't even know what it is exactly."
<br /><br />
Lasse: "You can't say, because they do that, we do that too.
If someone tortures a friend of yours, you don't go and torture one of his friends."
<br /><br />
Sigur: "Asshole."
<br /><br />
Lasse: "Okay, sorry, I withdraw the example."
<br /><br />
Sigur: "Crap!"
<br /><br />
Lasse: "Nope.
All right.
Maybe it was too drastic.
But then again, you've killed people..."
<br /><br />
Sigur hectically: "No, no.
You've tripped the general alarm!
The whole squadron of battle robots is moving out.
Shit.
All 15 of them... 20 ... 24 ... Twenty-seven going up right now...
...and set a course for us.
Shit.
Shit.
We're blown.
They know where we are."
<br /><br />
Lasse: "What do we do now?
Let's knock these things on the ground now!"
<br /><br />
Sigur: "Go into multimode. Quick!"
<br /><br />
Lasse: "Multimode?"
<br /><br />
Sigur: "mm --activate.
mm --flymode albatross."
<br /><br />
Lasse: "Okay, I did.
What is that?"
<br /><br />
Sigur: "I will send you my flying combat robot over.
It will automatically wedge itself behind you in Albatross mode.
These are experimental functions for formation flying and formation attacks."
Sigur's combat robot caught up with Lasses.
<br /><br />
Lasse: "And what do you do?"
<br /><br />
Sigur: "I'll take care of the others..."
<br /><br />
Lasse: "WHAT?
Drop it!
Let's get these things on the ground and then move on.
Sigur!"
<br /><br />
Sigur: "Just a moment."
</div>

Lasse hears hectic movements of Sigur, button and key presses through his headphones.
Then a keyboard storm.
A curse now and then.

<div class="terminal">
Lasse: "Sigur?
... Sigur?
... What are you doing?"
</div>
Sigur did not react.

<div class="terminal">
Lasse: "Sigur, we have an assignment.
We're supposed to expose the LBI's machinations, not go to war with them."
<br /><br />
Lasse: "I'm now flying with my two combat robots into the desert and throwing them on the ground."
</div>

Lasse turned off with both battle robots.

<div class="terminal">
Sigur: "Wait a minute.
I'm almost done."
<br /><br />
Lasse: "Finished with what?"
<br /><br />
Sigur: "So, now I've got her!"
<br /><br />
Lasse: "You brought them all down from heaven?"
<br /><br />
Sigur: "Nope."
</div>
He laughed.

<div class="terminal">
"Nah, she brought them all to me, all of them!
All 27 of them.
This is so cool when you deal with software that is not open source.
Sometimes there are such cheap bugs in it that you can completely control the program.
It's their own fault.
If the software was open source, this would not have happened.
I picked one after the other, automatically, and am now on my way to you in albatross formation flight.
27 battle robots, wedge-shaped, in front my lead Albatross.
It works great.
I love what they put in there."
<br /><br />
Lasse: "Have you gone mad?"
<br /><br />
Sigur: "Hey, what's wrong with you?
I rented some combat robots.
That's never been a problem for you before."
<br /><br />
Lasse: "But 27 at once!
An entire LBI squadron.
Now we're going to see some F22s move up."
<br /><br />
Sigur: "No problem.
There is an F22 fight mode.
I can make the fighting robots come down on an F22 like a bunch of hornets, or two or three at a time.
I wouldn't need more combat robots until I got four.
I've seen the simulations.
This is so cool."
<br /><br />
Lasse: "You're totally crazy!"
<br /><br />
Sigur: "Hey, Lasse.
Get over yourself.
I'm just trying to understand our mission.
The world public must be given credible information from us that the LBI exists.
How are we going to get enough attention and credibility if we don't put on a huge show?
We can't cooperate with big newspapers like WikiLeaks 2010 or Laura Poitras making a movie that will win an Oscar.
It's 2028, we're gonna have to bring in other guns.
And now we have good ordnance.
Great guns.
We can do something with them that will stand out.
29 battle robots, fully armed, that shouldn't even exist and that can fly in no-fly zones."
</div>

Lasse was silent.

<div class="terminal">
Sigur: "Will you come to me with your two?
We can use every combat robot we've got.
I have a plan."
</div>

Lasse got up, put his headset on the table and left the cabin.

<div class="terminal">
Sigur: "Lasse?
Are you there?
Lasse?"
</div>
