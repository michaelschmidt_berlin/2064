## Sigur
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Sigur sat in</span> a large, comfortable armchair in the bunker in front of five monitors of different sizes.
On one was WikiLeaks TV, on the other various news channels, a disabled combat robot control station, many terminal windows.
Tim stood behind him with folded arms.

Tim nodded: "Good text!
Good idea.
It's a good thing Anonymous had such a plan pretty much ready.
That'll work.
20 seconds left.
Does WikiLeaks shoot this right onto the screen?
Or do they watch it first?"

Sigur looked back briefly: "I have direct write access.
The umbrella is mine.
I can practically fade this right into CNN.
It will be a scrolling text, nice and slow to read along.
A little slower than necessary so that people can tweet on the side.
Ten more seconds."

Both looked spellbound at the monitors.

Tim: "5"

Sigur hits the enter key: "And go!"

BLENDE zur CNN-Redaktion /// On the WikiLeaks and CNN screens a Guy Fawkes mask appeared above the completely destroyed, smoking NSA main building and underneath it a text began to slowly move upwards:

<div class="terminal">

YES, WE CAN!

Whoever believed that the hacking power of Anonymous has diminished should take a close look today.
Today we have attacked and destroyed the centre of global surveillance with their own weapons.
We would like to answer two questions about this: How we did it and why.
<br /><br />
Flying combat robots have killed at least 782 000 people worldwide in the last 5 years.
It is the No.1 means of oppression in third world countries.
Whole populations are kept in fear by the constant invisible threat.
It's a decomposition strategy.
<br /><br />
We reject such an approach.
<br /><br />
For this reason we have decided to set an example with the weapons of the oppressors.
We have rented combat robots, the latest models, experimental combat robots, which can, for example, destroy F22 fighter planes completely on their own when in formation.
Fighting robots that, as you can see, can fly in no-fly zones.
<br /><br />
These are special combat robots.
You could also fly via Washington or New York, via London or Paris, via Tokyo, Canberra or Berlin.
And they have special armament with 15 times the explosive power of conventional missiles.
<br /><br />
They are not CIA combat robots, not American Air Force combat robots.
They are combat robots of a currently still unknown secret service called L B I.
<br /><br />
The LBI is everywhere, it operates worldwide. It has no headquarters.
Its members all work with second identities:
in governments, in corporations, in armies, in other intelligence agencies, in dummy corporations.
</div>

First journalist in front of the CNN monitor: "And now here come the crude conspiracy theories."

Second journalist: "Shall we turn that off?"

First journalist: "No.
It runs on WikiLeaks anyway.
Then it can work for us, too."

<div class="terminal">
They have the task of ensuring balance, the balance of hostile groups: in countries, in companies, armies, intelligence services.
Divide et impera is their main motto.
Separate people from each other, make them enemies of each other and make sure that there is a permanent war between them, a deadly balance, a rainbow war.
Just as the rainbow always seems to be the same in the sky, even though millions of raindrops fall, so people fall in the countries and the war remains.
A balance of death.
<br /><br />
In Khandahar 122 people died four months ago, including seven state presidents.
It was planned by the LBI as a mock attack by Russian combat robots, which were then to be repelled by American ones, by the famous five star.
This should strengthen America as a protective power in the region. But the five star did not come.
It was destroyed by battle robots controlled by us.
And so the fate took its course. No one was there to stop the battle robots, they fired their missiles.
<br /><br />
WikiLeaks.org is publishing all the log files of the attack as you read this.
These log files were also the reason why WikiLeaks provides us with this forum.
<br /><br />
In the course of our battle with the LBI, we also got their Bitcoin treasures: 1.2 million Bitcoins worth US$32.4 billion.
<br /><br />
We have distributed ten percent of this to the open source community, and we want to divide the rest equally among the countries whose presidents were killed in Khandahar. As restitution money. The transactions also take place during these minutes.
<br /><br />
Anonymous.
</div>
Sigur jumped up: "Horny. We did it!"
He clapped himself against Tim and shouted into his headset: "Lasse, Lasse!
Are you there?"

Lasse with his headset in front of the cabin: "Yes!"

Sigur: "Did you pass the Bitcoins?"

Lasse: "Yes, all gone.
I hope they are now confused for a while, so that we can manage to survive for a moment.
Otherwise, congratulations.
It went well.
No deaths have been reported yet and the log files and Bitcoins should be enough for the world to believe the story.
The LBI has guided missiles at Arab presidents.
That's quite a thing.
Some of us have a lot to digest right now.
I don't want to work at the LBI now."

Sigur: "Especially not if I work for a secret service at the same time.
I don't know how they like having a secret service within the secret service."

Lasse: "Political complications.
The countries of the seven dead presidents will probably meet soon and then the Middle East War will probably come.
Shit too, right?"

Sigur moaned: "Really shit.
They never learn.
They're just gonna start beating on each other.
Because there's no other way.
This is just revenge.
Like little kids.
There's nothing you can do.
They're blind.
And then they detonate the nuke... in self-defense.
They're so stupid.
I could scream."

Lasse: "We're not entirely innocent that this is happening now, are we?"

Sigur: "Shit, Lasse!
I'm not the one starting the war now.
If I had my way, you could simply shut down the LBI, turn the NSA headquarters into a memorial and the CIA into an adventure playground for hackers: a huge hackspace, with the original equipment.
I'm serious about it.
That would be awesome.
Then I'd move to Langley for a few years.
And then you would have to disarm all the nuclear missiles, remove the warhead and then use it to make a huge firework, send them on top of each other in the air.
Or fire patriot missiles at it.
Clay pigeon shooting on steroids.
But what will they do?
They'll turn an entire city into one big crater.
A hundred and fifty metres down.
They deliberately detonated the bomb on the ground first so they could get the pictures of the crater.
A few hundred thousand people then circled the earth in a cloud of dust for a few weeks."
He indicated that he might vomit.

Lasse: "Uahhh.
I get really sick..."

Figure: "And this is a game..."

A loud warning humming sounded.

Sigur: "Shit!
Yeah.
TRON.
Cancel that last sentence.
This is not a game here."

TRON: "Not just the last sentence."

Sigur stood up and looked around confused.

Lasse: "They are detonating the atomic bomb. Crater. 110m. Hundreds of thousands of people.
We can't know yet."

TRON: "Sigur?"

Sigur: "Yes.
No can do.
Please cancel everything since the atom bomb."

Sigur and Lasse waited a few minutes.

Sigur: "Okay.
I think we're getting out.
I'm glad that this was practically a private conversation."

Lasse: "I'm going with Roger now.
We'll clean up here and then we'll go to the meeting place."

Sigur: "Okay.
See you there.
Maybe.
If we survive this.
Good luck!"

Lasse: "You too!"
