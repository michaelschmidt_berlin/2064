## Wondrous
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Sigur looked defiantly strange</span> "Where did I screw up?"

[//]: # (# ########################### 1 #############################################################)
[//]: # (# - What's the difference between making mistakes and screwing up?)
[//]: # (# - What did Sigur not hear?)
[//]: # (# - What was "crap"?)

Wunderlich shrugged his shoulders: "I don't know.
But you have, otherwise you wouldn't be here with me, but with Lasse in the cafe.
He just ordered an ice cream shake."

Strangely Sigur grinned cautiously.

Sigur sat cross-legged in front of him: "This cannot be!
We accomplished the mission.
I accomplished the mission.
I've just been given two levels as a gift. Two!"

Wunderlich shrugged his shoulders again.
Weird. "So?"

Sigur: "So what?"

Curious: "Hmm.
You're sure you don't deserve to end up with me after being sooooo great and completing a mission.
But you're here."

Sigur: "That makes no sense!
You only come to you when you've done something wrong, not when you've accomplished a mission."

Strange: "Wrong!
Many who have just completed a mission come to me.
And nobody comes to me because he or she has done something wrong.
You don't know so much of the wisdom of the owl, huh?
Doing things wrong is the purpose of every school.
The <em> sense, you know?
You can only learn from things you do wrong.
And school is for learning, not for doing.
In general: If you do something right that you have not done wrong before, you don't know what is right about it.
If you do something right the first time, it's no use.
It's all owl wisdom.
You don't seem to know anything about it."
Strangely shook his head.
"And you want to go to level four, ha!"

Sigur: "And when do they come to you, if not when they've done something wrong?"

Strange: "When you've screwed up.
Like you."

Sigur breathed out loud: "And WHERE is the difference between screwing up and doing something wrong."

Miraculously his eyes rolled up: "Oh my God!
You don't know that?
Really?"

Sigur had a strangely stern look on his face. "No, I don't know..."

Strange: "You're here because you can't tell the difference, you blind sausage!"

Sigur swallowed.

Strange: "Shit is when you meet the owl for the first time and don't even listen to it.
The magic moment when you meet someone for the first time...
Never heard of it?"

"I WERE LISTENING!" Sigur went crazy.
"I listened carefully.
I've waited a long time to meet Tin.
I know how important what she says is.
Besides, I even listen to you."

"Just because you want to get out of here so badly...
Otherwise you wouldn't listen to me.
How you didn't listen to the owl, you sleepyhead."

Sigur looked at him with big eyes.
Sigur slowly: "WHAT Haven't I heard?
WHERE DID I SCREW UP?"

[//]: # (# ########################### 2 #############################################################)
[//]: # (# - Killing people only if there is immediate danger for another person, it is not possible to fight each other from level 3)
[//]: # (# - There are no final dead ends in life)
[//]: # (# - Law and justice do not coincide)

Strange: "You told the owl that you wanted to go to island level 4.
That's a strong statement for a rookie like you.
And you can't take that back so easily."

Sigur: "But I <emn> want</em> that!
I want to get to level 4.
Lasse is there too!"

Strange: "You can't get to level 4 that easily.
You don't have what it takes."

Wunderlich looked at a tiny wristwatch.
"You're registered for Level 2.
You illegally killed a man who was no immediate threat to anyone.
It's totally against every rule."

Sigur looked at him confused.

Wunderlich read from his watch and raised a finger: "Self-defence ... emergency shot only in the event of imminent danger to the life of another human being.
You can't violate something like that on level 3 and a hundred times not on level 4.
I'll tell you one thing, you're fucked!"

Sigur angry: "WHERE?
Where did I kill a man who didn't directly threaten anyone?"

Strange: "And you can't even go back to level 2 or something, or even get out of the academy.
You said you wanted to go to Level 4.
And that is true.
The owl has warned you."

Sigur: "Did not!"

Isn't it strange: "She did!"

Sigur: "Doesn't have it!"

Strange: "She said that you are not ready.
And you wiped that off and said, "You're ready.
The owl does not become clearer.
What the owl says is true - usually.
She says a few things about me that aren't completely true everywhere, I think."

Wunderlich looked briefly thoughtfully to the side.

Wunderling: "The owl never tells you what to do.
She always says only what is straight and how everything came about as we have it now.
Past and wisdom and all that.
Did you at least get that?
It's really, really easy."

Strangely, Sigur looked pitiful.
"You are now Cypherpunk.
You're big yourself.
You are free to strive for what you want to strive for.
Even if it's total nonsense, and you can never get there with the modest possibilities you have."

Sigur closed his eyes and clenched both fists.

Sigur: "WHERE have I killed a man?"

Curious: "One?
Three.
In the first of the helicopters that chased your flying combat robot."

Sigur: "That was FBI!
They were chasing me.
To the death.
They were gonna shoot rockets at me."

Strange: "To you?
Pah!
They were going to shoot your robot, not you.
If you were inside, and they were aiming right at you, I think you could have shot them.
Or maybe not.
I'm not sure.
The rule masters of the academy sometimes have strange views.
You'd better ask the owl himself.
But _you_ weren't in the combat robot at all.
And Lasse was far away.
You were in a bunker pushing buttons, that's all."

Sigur took a breath.
He thought of the other two helicopters, which he had also brought down from the sky shortly after the first one.

Sigur: "But this is war.
That was 2028.
There were different rules then than today.
It was normal at the time.
People die in war.
Those weren't civilians.
They made a distinction between civilians and soldiers back then."

Strange: "One must not kill people.
And if the rules were like that then, then they were wrong.
The laws back then had lots of bugs, we still fix bugs from that time.
When you kill a person, it doesn't matter if you see them as civilians or soldiers.
He's a human being.
And people can't be killed."

Sigur looked at him confused: "In war, one must not kill people?
Lasse also ..."

Wunderlich interrupted with a derogatory hand movement: "With such an attitude you will be sitting here for a long time.
Level 4?
Pah.
I'm laughing my head off.
Level three?
Forget it."

Wunderlich came closer with his head: "You have ver...shit!"

Sigur louder: "HAVE I NOT!"

Wunderlich: "Level 3 is about overcoming the opposition.
Do you get that?
The only thing you can be against is each other.
Being against it is not for cypherpunks, not from level 3.
The more I look at this:
You can kink the whole thing!
You're not gonna be able to do that at the Academy."
He gave him a big smile.

Sigur: "There is always a way out at the Cypherpunk Academy.
There are no final dead ends here.
Not at all.
Nowhere!"

[//]: # (# ########################### 3 #############################################################)
[//]: # (# - No violence, even if provoked)
[//]: # (# - Testing your limits promotes self-confidence)
[//]: # (# -)

Strange: "Here you go.
Then look around you!
Stone walls, about one meter thick, bars, no file, no hammer or anything else.
How are you gonna get out of here?
Maybe I could eat you up and carry you out through the bars in pieces."

Sigur tried to hit for Wunderlich, but he quickly scurried aside and hid behind a stone.

Wunderlich trembled all over his body: "Hey!
What are you doing?
I'm the only friend you have left.
Where is everybody else?
If you need them, they're not here.
And anyway, you go up against someone who's not an immediate threat.
That seems to be your sport.
Get a hold of yourself, or you won't be able to get out of here. Moron, you... Sacrifice."

Sigur's eyes became darker.
He slid to the wall behind him and leaned against it.

Sigur: "Shit ...
Yeah, you're right."

A tear pressed in his eye.
He looked frozen for a while.

Sigur: "Okay.
I made a mistake."

Wunderlich interrupted: "No mistake.
You fucked up!
You're a programmer.
You know the difference between machines and humans.
And where you cannot learn, you cannot make mistakes.
Hmm, sorry, you wouldn't understand something like that."

Sigur: "Yes, I understand that.
They're dead.
What am I to do?
Okay.
That was crap.
Am I supposed to stay here until the end of the game?
What sense would that make?"

Strange: "A lot of sense.
There'd be one less person out there killing people just like that.
It makes sense if you don't do anything from now on.
You can just stay here and wait for the game to end, you loser."

Sigur grabbed a stone and threatened to throw it.

Wunderlich crouched in exaggerated panic: "Stop!
Hold on!
Are you crazy?
I'm the only one who can help you get out of here.
And besides: Cypherpunks do not threaten anyone with violence.
Violence, that's against it, its high 10."

Sigur cut.
He put the stone back on the ground: "So you can help me after all?
You know a way out?"

Wunderlich, startled, held his mouth closed and stammered: "Hmmm.
Maybe.
More likely no.
When I look at you."

Sigur: "There is a way out.
I know that."

Wonderful: "Then show me where, wise guy. Or rather, "stupid."

Sigur clenched his fist and raised it, but then took it down again and looked at her.

Sigur: "Okay.
No more violence."

Wunderlich exhaled with relief.
"And how are you gonna do that?
There's so much broken down inside of you.
You're full of it, you fool, you donkey, you little shit!"

Sigur quietly: "Wonderful!
Tell me what I can do.
You have nothing more to fear from me."

Strange: "You never know.
One minute the sun is shining, the next minute you're throwing a rock at it.

Sigur: "No, really!
I get it.
Lasse once told me that about you:
You're testing my boundaries, aren't you?"

It's funny. "And it's pretty easy with you."
He looked at his watch.
"You're at minus 120 points.
No one in this dungeon has ever done that so quickly."

Sigur looked perceptively to the ground.

Wunderlich waited a while and came a little closer: "Good!
You asked what you can do?
For example, you can take a run-up here and jump against that stone there, the really big one.
Take the whole cell run, off the grid, and then fully against it.
And try to be really in the air when you hit the rock."

Sigur went to the stone and examined it.
He couldn't see anything special.

Sigur: "This stone here?"

Wunderlich nodded, "Or... maybe the other one next to it."
He looked doubtfully, but then immediately grinned and said: "No, the one you point to."

Sigur shook the stone.
He wasn't moving.
He struck it with his hand.
It seemed like a normal stone.
Then he went to the gate, wanted to take a run-up, but hesitated.
He beckoned.

Sigur: "That makes no sense.
It's just an ordinary stone.
A little clue should be there if that was an exit."

Wunderlich suddenly had a dented spoon in his hand and showed it to Sigur.
"Look, you know Neo from Zion?
Look, this is not a spoon!
There is no spoon.
Well?
This is not a stone.
Do you understand?"

Sigur moaned softly and nodded.
He took a run-up, jumped far and crashed with full force with his body and then head on the stone.
He bounced back and hit the stone floor with his arm.
His arm was burning in pain, his head was pounding, he was dizzy.
The stone had not moved a millimeter.
He got a throbbing headache and it made him slightly nauseous.

Sigur held his head and shouted, "Are you sure... it was the stone?"

Strange: "Yeah, sure.
This is the stone I was talking about.
Maybe you weren't fast enough."

Sigur looked at him in disbelief.

Wunderlich: "On the other hand, why should he move?
It's a stone, walled into a prison wall.
They usually do it so well that you can't just push him out by running into him."

Sigur: "But it is a ... more hidden Exit?"

Strange: "I didn't say that.
I think it's a normal stone."

In Sigur, anger was running high.
He went straight for Wunderlich.
He's hiding in a corner.

Sigur: "You said it's the stone, and if I jump against it ..."

Weird, "Then?"

"Then..."

Weird, "Then?"

Sigur: "I asked you what I could do..."

Strange: "Could you do it or not?
You did it.
You ran your full width into the wall.
You didn't ask what you could do to get out of here.
Only what you could do."

Sigur gasped for breath.
He clenched his fist.
Let her go.
Then his face went close to Wunderlichs and held his breath.
Wunderlich pressed himself against the wall, panicked and suddenly he spat Sigur right in the middle of his face and fled under him to the opposite corner.

Sigur, disgusted, wiped the spit from his face, jumped up, turned around and shouted with a clearly redder face: "YOU ...".
Then he stopped, breathed out and looked at Wunderlich questioningly.
He looked for some time.
Wunderlich looked back.
He blinked his eyes.
Sigur kept looking.
He was breathing calmly.
One minute, two minutes.

[//]: # (# ########################### 4 #############################################################)
[//]: # (# - Everyone is responsible for their own feelings)
[//]: # (# -)
[//]: # (# -)

Suddenly a soft "Pling!" sounded at the end of the corridor behind the grille.
An approximately hand-sized, glittering, colorfully painted glass ball jumped towards them, as if it were a matter of course, without bumping into them through the bars and towards Sigur.
He instinctively opened his hand and the ball jumped in.
It glowed in the most diverse shades of blue and green.
Sigur looked at her in amazement.

Sigur: "What is this?"

It's strange: "My, my.
They're going easy on you today.
That was pretty damn quick."

Sigur: "What is this?"

Strangely unimpressed: "A magic bullet, what else could it be?"

Sigur: "And what can you do with it?"

It's strange. "Throw it against the wall, for example."

Sigur looked at him angrily.

Strange: "No, really.
You have to throw it in a place where it could have meaning, and then it will turn that place into something meaningful."

Sigur: "In what?"

It's strange. "Into a window, for example."

Sigur struck out, but then stopped and looked at Wunderlich.

Sigur: "A window?
Why should I believe you?"

Wunderlich slowly said, "Into a window."

For a moment a window appeared on the sphere, with a wide view.

Sigur looked at the ball: "Well...
He threw her against the wall.

In an instant, at the spot where Sigur had thrown the ball, everything sputtered into millions of tiny, glowing dots of dust that slowly sank to the ground and disappeared into nothing.
A barred window appeared behind you with a magnificent view over a sun-drenched expansive landscape.

Sigur: "Wow!"

He went to the window, clasped the bars and looked outside.
He felt a cool wind blowing in and smiled at Wunderlich.
"Well, that's a first step.
Something is happening here.
I should have taken the other side, then we'd have the sun in the room now."

Strange: "Yes, I knew that.
But you wanted the other side."

Sigur without tension: "Why didn't you say that if you knew?"

Strange: "Because you did not _ask_.
You humans always think you're the smartest people in the universe.
I can tell you: Nah, it's not like that.
I'll tell you: Thinking with holes everywhere you look.
I've seen people here who thought even slower than you did.
Really.
And others were even more stupid, even less precise in their thinking."

Sigur closed his eyes and breathed calmly.
The anger rose again in his chest.
How could this little rat keep bringing him to the edge so quickly?
His chest was throbbing.
He put his hand there.

Strange: "You know, you're just not used to this.
I am an opera rat, the only species mentioned in the book "Cypherpunks".
Humans can't think of anything we opera rats can't overcome.
We always find a way in or out.
And that's hard for her to bear.
Because they think they're superior to us.
But you're not.
And that's why you're reacting so strongly.
You're really mad.
You tried to murder me.
And all because you can't stand the fact that I know more than you."

Sigur looked wonderfully serious and calm.
Then he went back to the window and checked the bars and joints of the window.
Everything seemed to be solid.

Sigur: "Now I know what you are doing.
I know what this is about now."

It's strange. "I'm curious."

Sigur: "You insult me, you trick me, you evaluate what I do.
And I'm getting feelings about it...
And then I want to do the same thing..."

Funny with a teacher's face, "That's why?"

Sigur: "I get feelings, then.
And those are my feelings.
It's got nothing to do with you.
That tells me something about me."

Wunderlich nodded with astonished expressions.

Sigur: "You can't create a feeling in me.
They arise in me because I am the way I am.
Lasse would feel differently."

[//]: # (# ########################### 5 #############################################################)
[//]: # (# - )
[//]: # (# -)
[//]: # (# -)

"Pling", it came from the end of the hall again and another bullet jumped at her.
Sigur opened his hand and she jumped in.

Sigur smiled.

Sigur: "And now what?"

Wunderlich: "Now we have sun in our room!
Throw it here!
Sun!"

A sun shone in the magic sphere.

Sigur looked at the ball.

Curious: "Sun.
Vitamin D.
This is food.
That's important in a dungeon like this.
Who knows how long we're gonna be in here.
And there will be sunshine for sure.
And it's good for the mood and for your skin too."

Sigur nodded and threw the ball at the opposite wall and in the same way a barred window appeared.
The sun blinded him directly.
He held his arms in front of his eyes.

Sigur: "Ah, nice.
It's good in the dark hole."

That's strange. "Told you the sun would come out."
He grinned broadly.

Sigur looked at the screen door: "But actually I could have thrown the ball to the screen door, couldn't I?"

Wunderlich nodded: "Then she would probably have jumped up and you could have gone outside.
I don't understand why you didn't.
Doors are usually the place where you go in and out.
And a magic bullet always does something useful, something that carries on.
I even told you that.
Well, I threw the magic bullet at the door the first time, not made some useless windows first."

Sigur laughed: "No, it would not have opened.
I had to make the mistake of not throwing them at the door, otherwise I wouldn't know exactly what would happen if I threw them against the wall."

[//]: # (# ########################### 6 #############################################################)
[//]: # (# - )
[//]: # (# -)
[//]: # (# -)

"Pling", it sounded from the end of the passage and a red-golden ball jumped along the passage, through the bars, into Sigur's hand.

Sigur grinned over to Wunderlich and showed the ball.

Wunderlich: "Then throw them on the floor now.
I think there's a secret passage you can use to get out."

Sigur smiled.

Strange: "You have to try everything so that you know what is happening.
You just said that.
If you throw the bullet now to the barred door and go outside, you don't know what will happen if you throw it on the floor, or to the ceiling, or into one of the windows."

Sigur smiled again and shook his head.

Sigur: "No, I don't have to."

It's strange: "And why not?"

Sigur: "I do not feel any urge to do that.
No pulse."

Strange: "But then you don't know whether there is a secret passage, or a rope comes down from the ceiling or the barred window opens ...
if you don't try this."

Sigur: "And I do not want to know.
That would be an answer to a question I don't have.
Owl wisdom.
Ever heard of it?"

Miraculously his pointed nose stuck up and said: "Maybe".

Sigur laughed and threw the ball to the gate.

[//]: # (# ########################### 7 #############################################################)
[//]: # (# - )
[//]: # (# -)
[//]: # (# -)

It crumbled like the first two and in the cloud the bars turned into shimmering blue light rods.
Sigur went to the staffs and checked them carefully with his finger.
He was gently pushed back.
He pushed harder and was pushed back harder.
He turned around and looked at Wunderlich.
He shrugged his shoulders and looked back with innocent eyes.

Sigur: "Come on, I know you know how to get through."

Wunderlich nods: "Yes, I know that.
But the way you're going, you won't believe me.
And don't do anything.
But you're prepared for it."

Sigur: "Tell me.
And no more games.
I want to get out of here now."

Strangely a little bashful: "Okay.
Then I'll tell you.
But thou shalt not throw the stone at me."

Sigur: "I'm not."

Isn't it strange, "Promise me."

Sigur: "Promised."

Wunderlich: "You have to start from here and then jump through at full speed and power.
And you must be in the air when you hit the bars."

Sigur closed his eyes.
He opened it, looked at Wunderlich: "I can't believe it!"

It's strange: "I mean what I say, I mean everything I say."

Sigur: "If I do this, will I get out of this dungeon here, or will it just knock me back?"

That's strange. "Through!"

Sigur: "Out or back?"

Weird. "Out."

Sigur pulled himself together, went to the other end of the dungeon.
He had a strange look on his face: "And what's beyond that?"

Strange: "I don't know.
It's late.
A little earlier would be the sphinx behind it before the third level.
But it's late."

Sigur: "What does late mean?"

Strange: "If you'd thrown the first bullet right, I guess there would have been the sphinx."

Sigur: "No, she wouldn't.
I wouldn't have gotten out of here without those two fake throws."

Sigur took a run-up and jumped onto the grid with all the strength he had.
A flash of light enveloped him, he felt weightless for a split second, he flew and landed in the Cypherpunk Cafe on the floor, right in front of the table where Lasse was sitting.
He bent down, patted Sigur on the shoulder and said: "Hi!
Well, what took you so long?"
