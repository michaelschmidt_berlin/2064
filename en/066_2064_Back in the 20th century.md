## Back in the 20th century
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">In Fortunato's beach house.</span> Fortunato was walking all over the beach apartment with his high red head.
Johnny was sitting in a chair.
Two men in black suits stood at the side of him, facing Fortunato and two monitors showing life-size images of the two agents Lasse had been talking to in San Diego.
A camera followed Fortunato automatically.

Fortunato: "This is a CATASTROPHE!
I give you two super hackers on a silver platter and you let them go.
Ha! You call the CIA slugs and the NSA snores and then two hackers escape you with a simple car exchange ...
The first one's already gone, and we haven't had a signal from the second one for 35 minutes."
He pointed to a display on the wall showing the names of Lasse and Sigur, and behind each of them a section of map with a question mark in the middle and a time.

First agent:
"He's in a mall in Phoenix.
It's our turn.
We now have 13 men down in this one department store.
And more than 50 are on their way there as a backup.
The whole complex is surrounded by police roadblocks.
They check every car, every pedestrian.
We know what he looks like.
He can't get out of there.
No problem."

Fortunato: "You guys still haven't grasped what it's all about.
This is war.
We have the greatest network of information sources available, around the world, live feeds, information about every citizen of the earth who has ever been on the Internet, 24 hours a day, 7 days a week.
And the most important of these we get under cover.
Not only under cover of our enemies, no, also under cover of CIA, NSA, most of the government.
There are maybe 11 or 12 people in Washington who know about us.
That's why we have to protect the cover of our people!
That's why we pay them with Bitcoins.
In 2028, something like that doesn't cost dollars or euros, it costs Bitcoins, many Bitcoins.
On Monday, the reserves are exhausted.
Away!
Then we can no longer serve our contacts.
We can't just transfer money to it, we can't take cash anywhere, we can't buy Bitcoins for cash.
We need too much.
We hold over 12% of all Bitcoins that exist.
Yes!
And 11.8 percent are now in the hands of a 20-year-old man walking around a department store with the sole purpose of getting away from you."

Second agent: "We are breaking off the pursuit.
We'll get him.
Take him to a suitable place and then I will personally convince him to disarm the bombs and return the Bitcoins.
He may die in the process.
But not until we get the Bitcoins back.
I could be in Phoenix in three hours.
You'll have the money in six hours..."

Fortunato with a slightly red head: "... or the shop has blown up in our faces.
I don't want to wake up and find the Bitcoins gone.
Then we can shut this thing down!
I just told you that.
Is anyone here listening to me?
There must be another way.

You still can't get into their laptops?
You still don't know where the bombs are?
I mean, this is the gate net, we know the gate net, but we're blind here, completely blind.
We don't know what they are talking about, we don't know what they are doing, we don't know anything, we can't find the bombs, nothing, nothing, nothing.
And now both transmitters are also silent, we don't even have a tracking system.
Are we back in the 20th century now?"

First Agent: "I'm getting a message in...
He's not at the mall anymore.
They've searched the whole place.
He must have gotten through the barriers somehow.
We've lost him!"

Fortunato: "WAAASSS?" His face was powder red.
"NO! DEPPING! INABLE!
They'll castrate you!"

He closed his eyes and then suddenly screamed:
"S C H E I S S E !"
He kicked against one of the large screens, which fell backwards and shattered into thousands of splinters.
He screamed at the agents on the remaining monitor:
"Change of plans! Change of plans. I want him here, I want them both here NOW!
We'll do it the old-fashioned way.
Not that super-secret bureaucratic bullshit with hundreds of lame asses.
Move in!
ACCESS!
Did you hear that? Get her!
Both.
Bring her over here.
I want her in this room before sundown today.
I won't stand for it any longer.
We're gonna do it like we've always done."
