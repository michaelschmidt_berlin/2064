## The Swastika Man

<div class="_19xx">
    <span class="year">1922</span>
    <hr class="hr" />
</div>

### SCENE 9: In the same vaulted cellar

____
The captain, Berthold, another uniformed man with a swastika bandage on his upper arm.
You stand in the middle of the room.
Hilde and Wilhelm are no longer there.

Swastika man: "So!
So this is your room for advanced interrogation."
He looked around and nodded.
"Nice place.
I like it!
Advanced interrogation techniques... good name.
I like it too!
You have to know what these things are called.
The people want to be seduced.
It has a taste for being seduced."

Hauptmann: "If you choose the order of the extended interrogation methods well, they talk pretty fast.
The Papenburgs were here, both together in this room, with both of us."

He smiled.

"It happened very quickly.
And in the end, they both told us everything we wanted to know."

Swastika man: "And then you took her out of the game.
It was a great deed!
We even celebrated that in Munich.
The photo with the two of them, swimming in the canal... face down...
The red bastards must be eradicated.
There is no other way.
They cannot be converted."

Hauptmann: "And before that they have to be given special treatment.
Information is important."

Swastika man: "A technical question: You have to know in advance what they know, otherwise it is difficult, isn't it?"

Captain: "Then it takes longer.
Eventually they'll talk."

Swastika man: "But then what are they talking about?
If such a poor bird doesn't know anything, then at some point he starts to invent something.
We have a serious problem with this.
The whole thing is missing something."

Captain: "What?"

Hakenkreuzmann: "We talk about it a lot in Munich.
Somewhere where he can exhale once in a while.
Getting a little strength.
We were thinking maybe some good guy to nurse him back up in between.
A friend.
Someone he can catch his breath with.
Then he gets hope.
If you then extend that over a few weeks or months, I think you can get quite a lot out of it."

Captain thoughtful: "Hmmm. Yeah, that might be true."
Berthold nodded in agreement.

Swastika man: "Illusions, you need illusions everywhere.
Illusions of good, that gives hope.
Hope is good.
That makes you weak.
Illusions of evil.
That's scary.
Hope and fear.
A lot can be done with these two things.
It is important how things look like, how things are called.
We are currently building up a powerful propaganda department in Munich.
Hitler does that himself.
Do you know him?"

Captain: "No."

Swastika man: "Great man.
Very good language.
Powerful voice.
And if something doesn't work out, he talked for a while, and in the end everybody thinks that it went well anyway.
He gets everybody, everybody...
Keeps out of all fights, but with his mouth he ironed all flat.
This is an important thing.
Words!"

Captain: "I see.
That sounds good.
I wish we had a real leader here."

Swastika man: "We will soon be coming to Berlin.
We need people like you."

Captain nodded and looked at Berthold: "We're in."
Berthold nodded back.
____
