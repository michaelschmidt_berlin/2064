## The warrant
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">In Marlene's old classroom</span>, two years after her escape.

Anni ran with a red face and her smartphone in her hand behind the last row of desks in the classroom.
Sophie followed her, put a hand on her shoulder.
Anni got loose.

Anni: "What a fucker!"
She stamped her foot on the floor.
"I can't believe it!
He did it again!
I thought that was over for good.
I thought they would have plugged the holes by now.
I have the latest iOS version."
She reached out her smartphone to Sophie.

Sophie took it and looked at the display: "Shit!
What a bummer!
He's posting on your Facebook account again.
Shit.
To all your friends.
And such bullshit!
He's so full of shit.
You really should never have gotten involved with him.
He was already an asshole before this.
After all, there are no photos this time ... Oh shit...
Nope."

Annie: "What?"

Sophie: "A photo.
Right now."

Anni: "Give me!!"

Sophie held the smartphone away:
"Nope.
Better not watch this."

Annie: "But not naked again?"
She tore the smartphone from Sophie's hand, looked at it and leaned against the wall.
Then she slowly let herself slide down: "Oh, no...
No...
No."
She screamed at the smartphone, "You fuck, ass, birdbrain."

She put her head in her hands and cried.
Sophie carefully put her hand on her back: "Come on, now we're really going to do it.
We'll report him!
You know, real police and all.
Everyone knows he did it.
He has done it before and bragged about it.
We'll get him."

Anni sobbed: "It could have been anyone.
It's some kind of fucking weakness from some shitty YouTube tutorial.
And even if we did get him, the photos are out.
I'll never get them back."
Sophie stroked her back.

Anni sobbed: "Why?
Why me again?
Why still?
I thought it was over.
I did not pick up my smartphone for three months last year.
Until everyone said, "There are no more back doors now."

Kevin came up to them from behind: "Yes, yes, they said that:
The most private and secure iPhone ever ... super transparent and secure Android phones like never before.
No more back doors for the first time.
None at all...
Never again...
Apple and Google celebrate their liberation from the secret services.
Big show.
All thumbs up.
But then they installed new back doors again.
They can't help doing it.
Maybe they're being forced.
Throw away your iPhone!
As long as it's not open source, you can forget it."

Sophie pushed him with her elbow: "Hey, you fool, can't you see she's in a bad way?
She doesn't need a lecture now."

Anni wiped the tears from her eyes: 'Leave him.
He's right.
That's what I do now.
Get rid of it.
Marlene said that a few years ago.
She always said that there are back doors and all sorts of things in there and that they monitor us with them.
Oh, we could use them now.
She'd know what we could do now."

Kevin: "Yes.
She'd chop the stinker.
He would see his picture constantly on every screen at school, for weeks, and nowhere would he be able to access his own Facebook account.
He wouldn't have a nice life anymore."

Sophie laughed: "Yes!
Marlene.
That would be cool!
...
That's funny.
She never got back to me.
We were actually good with her, right?
Is she still traveling the world with her brother?"

Kevin: "No idea.
The latest Twitter messages are more than a year old.
And they were pretty weird.
Only beach and sun, no laptops...
Whether she was there with her brother, I don't know."

Sophie: "The parents said that they got problems with secret services and wanted to stop hacking."

Kevin: "Do you believe that?
Marlene?
Give up hacking?
A cat goes vegan?
Marlene remorsefully realizes that hacking is not good, that it is not done in our world and becomes "reasonable".
"They'd have to torture her or something, brainwash her..."

Sophie: "Uh-huh.
You're right.
But then what does she do?
Then where is it?
What's up with her?
We really needed her."

Kevin shrugged, "I don't know. Wait."
He turned to a boy in the second row: "Tobias!"

Tobias: "Yep!"

Kevin: "You were at one of those young hacker days at CCC a few months ago, or something?"

Tobias: "Yep!"

Kevin: "Did they say anything about Marlene?"

Tobias: "Nah, nothing new."

Kevin: "Nothing?"

Tobias printed a little: "One of the CCC mentioned them.
But nothing special."

Kevin: "Tobias! What? Information..."

Tobias hesitantly: "There was a dispute about something.
Someone said it's not right.
I asked someone who had defended her about it, what it was, and he said Marlene was still looking for her brother.
But she would not only have friends among the hackers."

Kevin: "So she is still active there!
Fuck!
Why didn't you tell me?
Here in class, and all.
Where is she now?"

Tobias: "The hacker told me to keep this to myself.
She wouldn't be out in public right now."

Kevin: "Where is she?"

Tobias grumbled.

Kevin walked up to him in the second row, "Where is she?"
Sophie followed him.

Tobias: "I do not know.
I asked him too, but he didn't tell me anything.
But I had a feeling he knew that.
I'm not supposed to talk about it!"

Sophie: "So why are you talking about it?"

The door flew open.
Luke stormed into the classroom and stood in the middle: "HEY!
HAMMER!
HAMMER!
Have you heard?
Marlene is wanted internationally, with an arrest warrant.
They say she did this, they say she hacked the vulnerability database!
It was her!!!"
He waved his tablet.

The three of them rushed towards Luke: "Show!
Show!" They bent over the tablet and read:


<div class="newspaper">
____
*German hacker exposed in NSA spy scandal*
<br /><br />
The Berlin hacker Marlene Farras (18) is said to be largely responsible for last year's momentous revelations about weaknesses in computer systems.
She has been on the run for over two years for other crimes.
Last night, the United States government issued an international arrest warrant against her.
The USA accuses her of having betrayed military secrets, which have led to millions of thefts, fraud, violation of copyright and personal rights and in three proven cases to murder.
She is to be charged under the Espionage Act of 1917.
Should she be convicted, she faces a life sentence or the death penalty in the USA.
<br /><br />
Federal Justice Minister Lamprecht said: "We reject the death penalty and will only extradite them if we get a guarantee from the USA.
But unfortunately in this case the legal situation is otherwise clear.
She didn't work at the NSA, so she's not a whistleblower.
It appears that she, assisted by a larger group of hackers, hacked into the NSA from the outside and stole the vulnerability database.
This is computer violence, a clear case of espionage.
As far as we know, she wanted to extort money with it at first.
When this failed, she published the database in a completely irresponsible way.
We all know the consequences.
Infinite number of violations of personal rights, property rights, industrial espionage, some companies had to declare bankruptcy. Three people are known to have died."
____
</div>

Anni: "Woah!
Whoa!
Woahwoahwoah!
Who are these jerks?"

Sophie: "They want to blame Marlene for murders.
Spying?
Are they out of their minds?
What is that?"

Anni: "Sure, I also thought about killing the asshole who posted my pictures.
But it wouldn't be Marlene's fault.
Are you out of your mind?
I don't believe it.
What is this shit?"

Kevin: "Shit-for-brains.
Marlene? Extort money?
Totally off.
They just write that, they don't know anything about her.
They're so stupid... Marlene wouldn't put herself in danger for money?
Marlene, for money?
They are so blind."

Sophie: "This is propaganda.
There's something bigger behind this.
They shoot at them.
I'm pretty sure she hacked the database.
Also that she published them.
It's just as well that we know.
She probably did it to give them a good smack.
The ones who put all the flaws in there in the first place: Secret services, corporations.
And the message seems to have gotten through.
Looks like she got them dead to rights.
And now she's going to jail for this?
Or worse.
The heads of the secret services, the corporate bosses, the American president in prison, yes, that would be fair.
But Marlene?"

Kevin: "Kill the Messenger!"

Anni clenched a fist: "Hey, guys!
We have to do something about it!
We can't just accept this.
This needs an answer.
And a loud one at that.
This can't just stand there.
Marlene's from our class.
I know a YouTube blogger...
She's blogging from the apartment next door."

Kevin determined: "Yes!
They can't get away with this.
No way."

Sophie: "We've got to piss those fuckers off for real!"

Anni and Kevin looked at Sophie a little surprised and then nodded.
