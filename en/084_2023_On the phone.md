## On the phone
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Luigi at a telephone</span> in an Internet cafe in Lecco on Lake Garda, about two hours' drive from Luino.

Luigi: "They_ send someone to us who is in the public eye, someone well known.
Preferably with a camera team.
No, definitely with a camera crew.
Airport Bolzano.
Tomorrow morning at 10:30... Yes, it's necessary...


Amelie? Out of Berlin? Yeah, that would be great!


She'll be there, absolutely, you can count on it... call the Courage Foundation and get confirmation that this is real.
They're just around the corner from you in Berlin. Yeah, I'm coming too.
No one else... See you tomorrow."
