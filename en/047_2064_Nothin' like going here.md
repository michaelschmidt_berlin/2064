## Let's get out of here!

<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">In a cab.</span>

Stop the car!" Lasse yelled at the driver.
Sigur choked next to him.
He opened the door, stumbled out and vomited.
Then he crouched for a few moments on the spot.
Lasse came from behind and picked him up: "Come on, think something when we really get out of here, sit in the plane, go home ... come, just a little more..."
He pulled him back into the cab.

Sigur was as white as chalk, his eyes blacked out.
He kept clenching his fists and shaking his head.

Lasse: "Come on, hang in there, ten more minutes."

The driver did not drive to the main entrance of the airport, but through a service lock.
A man with a baggage car was waiting for her behind it.
Lasse and Sigur took their backpacks and climbed onto the wagon.
The man handed them a bottle of water without a word, which Sigur liked to take, nodded to them and drove them straight to their gate, where a steward was waiting at the stairs.

Steward: "No suitcases?
All the easier.
Follow me."

They went up the stairs together, nobody seemed to care about them.
Lasse and Sigur found a place on a bench in front of the gate counter and dropped onto the bench.
Back in the normal world.
People waiting for a flight, immersed in smartphones, one had a laptop with Facebook.
The display above the flight switch said:

Expected departure time: 13:35.

Lasse shook his head and said quietly: "An hour later than planned ... Fortunato".

Sigur leaned his head against Lasse's shoulder and closed his eyes.

"Sigur has never done this before," Lasse thought and put his arm around him.

After about three quarters of an hour the flight was called.
They stood in line in front of the gate switch.
Lasse showed ID and ticket, was waved through.
Sigur was about to do the same when a man in a suit grabbed his shoulder from behind.
Sigur turned into panic.
Two giant security guards stood in front of him, bodybuilders with buttons and curled ears.
Another one walked with big steps towards Lasse, who had turned around.

Security guard to Sigur: "We still have a few questions."

Sigur held his breath, panicked and thought, "Where's an escape route?"
He looked left and right.
But the second security guard was already behind him and grabbed him firmly by the shoulder.

Second security guard: "Come with me, please."

Lasse stormed off towards Sigur, but the security guard in front of him intercepted him.
In the arms of the security guard, he stared panically in the direction in which Sigur had just been taken away, but he could not get away.

Third security guard to Lasse: "Easy!
Shut up! Shut up!
It's no use anyway.
Get on the plane!
There's nothing for you here."

Lasse tried everything to get away.
He yelled at him, but the man held him tight.
Only when the other two men with Sigur had disappeared behind a door did he let him go.
Lasse ran towards the door and shook the handle.
No one seemed to notice him.
He kicked it.
Nothing. Nothing.
Only a few passengers looked in astonishment as they passed by.

He leaned against the wall and slowly slid down until he sat.
Then he put his hands in front of his face.


