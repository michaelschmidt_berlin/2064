## The Cypherpunk Academy
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">After a one-hour flight</span> the plane landed at a regional airport.
Sigur and Tim got out.
Next to the runway, the huge white balloon hovered about 20 meters above the ground.
He cast shadows over the whole scene.
Its outer skin seemed to be made of harder material than normal balloons, maybe hard<x> plastic or even metal.
He looked heavy and did not give the impression that he could fly.
He also had no actual basket at the bottom, just some kind of weight.
Sigur was again amazed at his sheer size.
Besides the weight he discovered a shaft that seemed to lead into the balloon and from which two blue ropes hung down to the ground.
Underneath Lasse waited with a broad grin.

Sigur ran off and fell into Lasse's arms.

Lasse: "Hey!
So good to see you!
You cyberterrorist!"

Sigur: "Yourself!"
They laughed and clapped their hands.
Tim came along and Lasse hugged him.

Sigur: "That was an action!
Unbelievable!
I have never sweated so much and so often on any mission.
I didn't expect to see you in the game again.
And the ride over here with Tim was amazing..."

Lasse exhaled.
"And then I'll meet you here."
He pointed at the balloon.
"You see the balloon, right?"

Sigur: "Yeah, sure.
It's big enough.
How can I miss him?
What is that?
Tim didn't see him earlier..."

Lasse turned to Tim and pointed to the balloon. "Tim, do you see the balloon?"

Tim: "What balloon again?
There's an airport, a tarmac, a few meadows behind it.
Are you starting now?
Why do you ask?"

Sigur amazed.
"But don't you see we're in the shadows?
Where did he come from?"

Tim: "Hey, now stop it!
There's no shadow here.
The sun blinds me."
And he actually squinted his eyes a little.

Sigur looked astonished at Lasse: "What is this?
Is he blind?"

Lasse shook his head: "No, not at all.
The balloon is the Cypherpunk Academy.
You can only see them if you're Cypherpunk."

Sigur: "_That_ is the Cypherpunk Academy?
Wicked!
But I'm not..."

Lasse: "Yes, I do!
You are.
But you can call it something else if you want to."
He proudly slapped him on the shoulder: "Congratulations.
Come on, grab a rope.
We must get to the Sphinx."

Sigur shook his head and took one of the ropes: "And if I don't want to be a cypher punk..."

Lasse: "Yes, you do.
Trust me.
Put your foot in the loop at the bottom like this.
And then touch here."

Sigur: "First we say goodbye to Tim."

Lasse: "You don't need to!
That's what TRON does.
Everything that we do with the ropes, Tim doesn't even see anymore.
TRON makes us say goodbye in the normal way, or anything else.
It's probably already happened.
You can watch it later."

Sigur looked at him astonished.
Lasse winked at him, put his foot in the rope and rushed up.
After a few seconds he disappeared into the shaft of the balloon.
Sigur looked once more at Tim, who had already turned away and went back towards the plane.
He was talking to someone invisible.

Sigur carefully put his foot in the loop, looked up and held on tight.
Then the rope made a jerk of its own accord and he rushed up.
His hands were like stuck, his legs were pressed through by some strange force.
He was getting faster and faster.
The shaft was pitch black.
He swept up further.
After some time he saw individual dots of colour appearing on the shaft wall around him, then they started to move, grew more and bigger and finally everything turned into a sea of colourful leaves, small fish and other things that floated and flowed in confusion.
He went through the roof.
The rope disappeared from his hand and he landed on a platform in the middle of a huge dome.
The platform was about five meters in diameter and fell vertically downwards on all sides.
He didn't see the ground.
To his right, Lasse stood on a similar plateau.
Sigur raised his thumb to him, but Lasse looked back only briefly and did not answer.
Lasse looked concentrated to a spot far in front of him on the dome wall, as if something would come out of there at any moment.
He went slightly to his knees and spread his arms.

"Welcome to the academy, Sigur," said a voice next to him and he turned to her.
Mostafa, the Mosquito Fly, fluttered in front of his face and looked at him friendly.
"I am Mostafa!
We met at the orientation meeting.

Sigur nodded.

Mostafa: "Lasse is already waiting for the sphinx.
That's always dangerous.
With the Sphinx, you never know what's gonna happen.
You really have to be careful."

They looked at Lasse for a while.

Mostafa: "You're the one who did the first LBI mission with Lasse, right?"

Sigur: "Right! And you can ask anything about Cypherpunk Academy, right?"

Mostafa: "Not only that.
I am also the teacher for reading and writing.
Of course the reading and writing of code, of programs.
Cypherpunks write programs.
It's kind of the one-time thing here.
That's why we have the Cypherpunk computer playground."

Sigur: "I can program."

Mostafa: "I know.
That's why we don't go to the playground right away.
But there's a lot you don't know about them.
You'll get that later."

Sigur looked over again to Lasse, who made strange evasive movements: "Do I have to go to the sphinx too?"

Mostafa: "You don't have to.
Here at the academy, you never have to do anything."
He smiled.
"You have to if you want to get to the interesting places.
The Sphinx guards all entrances and exits in the Academy.
You'll have to pass her again and again, or you won't even get into the Academy."

Sigur: "How?
This isn't in here yet?"

He pointed to the huge domed hall, which in his eyes had to be about as big as the whole balloon.
He looked to the right and was startled.
Lasse had suddenly disappeared and where his platform had been before, there was now a huge boulder with an oversized beehive in the middle.
On top of it sat Wim, the bear, and looked over to them with calm eyes.

Sigur was amazed: "What...?
Where is...?"

"In a parallel world.
The Academy consists of many parallel worlds.
Come along," Mostafa said and took off.
"Wim is waiting for you."

"How am I supposed to get over there?
I can't fly and it's too far to jump," Sigur shouted behind him and looked down the edge of the platform.

Mostafa shouted from the other side: "In the Cypherpunk Academy everything goes with thinking.
Think yourself there!"

Sigur nodded to Mostafa: "I understand."
He took a step back, put his hands to the side of his forehead and said softly, "To the bear, to the bear."
Nothing moved.

Mostafa: "Words are not thinking.
You have to let go.
You have to think right.
Trust!"

Sigur tried it a few more times and again nothing happened.

Mostafa flew over to him again: "Well?
Backfire?
You don't have to think that you want to go there, but think that you're there."

Sigur's ears were popping, a sting went through his eyes, he pinched them, and when he opened his eyes again he was standing right in front of the bear's rock.
He felt dizzy.
He stumbled to the right and could barely catch himself.
Mostafa flew under his arm and supported him.

Mostafa: "No problem, no problem.
This is always on the first teleport.
You're going to feel sick.
Your brain registers quite slowly that you are now somewhere else without having gone there.
It doesn't know that yet."

Sigur grabbed his stomach.
He knelt down.

Wim jumped down from the beehive and sat cross-legged in front of Sigur on the floor.
He straightened up after a while and sat down too.
A pleasant atmosphere emanated from Wim.
He looked at Sigur with alert, interested eyes and an invisible smile.
Sigur's body relaxed.
He felt lighter.
The efforts of the last few days fell away from him.

"Welcome to the academy, Sigur," hummed the bear with a dark voice.
"This stone marks the entrance to the islands of the plains.
You'll see why they're called that.
There are 11 of them."

Sigur: "Eleven? As many as basic ideas?"

Wim: "Right.
And above the eleventh level floats my garden.
The Garden of Epicurus.
That's the goal.
It's gonna take you a while to get there.
The task is to find the way there.

On your way across the 11 levels you will find many heavily fortified cities, each with a temple in the center.
You play a knight in the early Middle Ages.
The temples are the guardians of the paths, they watch over who can walk which way to the next city, or to a forest or a river.
They all have a mystery, each one their own, it is a cultural thought error that can be solved in different ways.
A mental defect that prevents you from becoming a cypherpunk.
You have to solve it.
And not just in one way, but in at least four.
You can also add your own solutions later, with a pull request, and they can become part of a temple puzzle.
Thus you can become a candidate, a helper and in the end a servant of a temple.
Temple servant is the highest you can achieve.
There are no temple chiefs or priests or anything like that.

<div class="infobox" id="pullrequest"></div>

If you don't have a city rights yet, you must conquer a city first, with weapons of the past and not alone.
That won't do.
There are always attacks from passing armies that you can join.
And you need skills to be taken by an army.
You get such skills especially when you solve programming and hacking tasks.
When you arrive at the 11th level, you will be able to program very well.
And hack that the Command Center Taurus gets all scared and anxious."

Sigur: "I can program and hack."

Wim grinned: "Yes and no.
You'll see."

Sigur looked at Wim sceptically.

Wim: "Now you will meet the next teacher of the academy.
He's waiting at the entrance to the islands of the plains.
This is here!"

Wim showed next to him.

Sigur nodded. He looked at the place Wim was pointing.

Sigur: "Where?"

Wim: "Are you ready?"

Sigur: "To go in?
Yes!"

Wim: "Are you ready for what's coming."

Sigur: "What's coming?"

Wim: "That which comes, that comes.
Look around you!
Is there anything dangerous around you?
Something threatens your life."

Sigur looked around, shook his head.

Wim: "If something feels unsafe, focus on it."

Sigur closed his eyes.
Opened it again: "Nothing."

Wim: "Ask yourself: You have everything you need.
Is there something you're waiting for, something you need before you can move on?
Anything you need to know? Or clarify?
Or can you just be here as you are?"

Sigur closed his eyes again and breathed calmly.
He nodded.

Sigur: "What's next?"

But Wim had already disappeared, the beehive too, and Sigur's platform was gone too.
The whole dome changed at breathtaking speed.
Sigur instinctively stood up and took a step back.
He looked on all sides.
It now stood on a rocky plain that reached to the edge of the dome on all sides.
Everything became darker, got sharper edges and contours and suddenly a gloomy rumble could be heard throughout the dome.
The whole room was shaking, then suddenly a huge circular area of the dome was on fire.
Sigur was staring there.
Some flames struck far into the dome.
All of a sudden a dark figure with a bird's head and huge wings shot out of the middle of the circle at high speed and flew up to the dome roof, then from there directly towards Sigur.
She let about a dozen egg-shaped, burning structures fly towards Sigur.
He threw himself on the ground, turned to the side and just managed to avoid being hit.

Sigur: "Shit, the Sphinx!"

The sphinx landed on the ground about twenty meters in front of him and grew bigger and bigger and bigger until it took up about a third of the height of the dome.

Sigur said softly: "The Sphinx!"

She bent down to him and asked in a loud, rumbling voice:

"WHO ARE YOU TO DARE COME BEFORE ME?"

Sigur straightened up and answered without hesitation: "A man!"
He knew he had to answer fast.
About a third of a second to beat the Sphinx.
That was his three second question... gone.

The Sphinx flapped her wings and cried out in pain.
Then she gathered her strength and put on the wings.
She had shrunk a visible piece and looked at Sigur darker and more sternly.

Sphinx: "WHAT DO YOU WANT TO DO BACK THIS GATE?"

Sigur: "Learn.
I want to learn."

The Sphinx cried out for the second time, shaking his head.
Sigur looked at her highly concentrated.
Whenever he would answer "right", she would lose strength and become angrier.
"Right" meant right from their point of view.
It was important, Lasse had told him.
The Sphinx thought he was in possession of all the wisdom of the world, the complete truth.
To defeat the Sphinx, you had to understand how she thought.

The sphinx calmed down again, bent to Sigur and turned her head threateningly back and forth:

Sphinx: "WHAT MOST YOU NEED HERE?"

Sigur: "Love of justice"

"That was easy," thought Sigur, "The enemies of the Internet in 2028 were out to divide the world into people who lived in prosperity and those who were supposed to work for them.

The cry of the sphinx became more and more like a howl.
Her eyes became darker, she began to lose considerable strength, her wings became stiffer, she continued to shrink.

Sphinx: "WHAT IS STRENGTHER THAN ALL THE SECRET SERVICES AND ARMS AND COMPANIES OF THIS WORLD?

Sigur cut a short moment.
It had known it once.
It slipped his mind.
But it had been so easy.
What was that?
He concentrated.
The Sphinx began to puff up.

Sigur: "Youth!" He breathed out.
That was a close call.
You had to answer within three seconds, otherwise it was too late.

The Sphinx swooshed with her wings.

Sphinx: "WHAT DO YOU DO WHEN YOU FAIL?"

Sigur: "I cannot fail.
When I make mistakes, I learn. When I do something right, I rejoice."

The Sphinx sighed, loud and pitiful.
Her voice became fragile.
She shook slightly.

Sphinx: "YOU WILL LOSE YOU HERE!"

Sigur nodded clearly.

"And that's what I want," he said with a big grin.

A dark, warm voice sounded from the background:

TRON: "Say it again another way."

Sigur Serious: "I want to lose myself to find me."

A pitiful croak came from the beak of the sphinx.
She crouched on the floor and turned in pain.
Then she straightened up again with her last ounce of strength.

Sphinx: "YOU CANNOT WIN ON ME."

That was a trick question.
The word "against" was the decisive factor.
Sigur said triumphantly: "But I can win. Over and over again!"

He jumped off his pedestal, ran towards the sphinx, which had turned to stone, and gave it a kick.
Everything crumbled to dust and was gradually carried out by an emerging wind.

The whole hall changed again.
Sigur saw a medieval town developing around him.
He stood on the main square in the middle of the city, in front of him a large temple of finely worked red sandstone and marble.
Everywhere people were passing by who were doing some kind of job.
From their clothes you could guess what they were doing.
At the edge of the square one could see a row of houses and shops.
On the right, a little further away there is a market place and further back a city gate.

From behind a clear, soft voice said, "Sigur!" He turned around and in front of him sat on a wooden bench, Tin, the owl.

Sigur bowed.

Tin: "I welcome you too."

Sigur: "Is this one of those temples that Wim was talking about?
Why am I here already?
I wanted to get to the entrance, cities have to be conquered."

Tin: "You are in the practice room.
This is a city where you are a temple servant, the highest thing you can become in a city.
Here you can come here and see what you can achieve.
But it's all a bit hidden, too.
You also need to find out what's out there.
But it's all there in the game.
There's weapons, clothing, other equipment, maps.
Everyone who walks by here knows something or can do something.
At the market you can get remedies and gems, special metals.
You'll understand when you need it.
But the most important thing is this."

Tin nodded towards the temple.

Sigur nodded too.
Sigur: "Sure.
What can I do here?"

Tin: "As a temple servant, you do not have to enter the temple to do anything with it.
You can open a window to the temple from anywhere, even in other cities.
Remember the temple and stroke the air like this."

Tin painted a half circle in the air with his hand.
Sigur imitated the movement.

A picture frame about three meters wide and 1.5 meters high appeared in front of them at a height of about one meter.
He showed the sentence:

<div class="terminal">
You have to adapt if you want to achieve something in life, if you want to be successful.
But what you have to do without because you adapt, you can compensate for with other things.
For example by having a lot of money, the most beautiful house, the most interesting partner, the most exciting journeys to make or something else exciting or beautiful.
</div>

At the same time the same sentence lit up on the front of the temple.

Sigur smiled: "Or the best computer ...
But there's something to it.
You have to adapt a little bit, don't you?"

Tin looked at him questioningly.

Sigur: "A little.
You can't do everything the way you want."

Tin: "You never have to adapt.
It depends on whether you manage to want something you might not want before.
You only have to adapt when you do something you do not want to do.
Then that's a betrayal of yourself.
There are many people who can't even do that.

In the real game, what you read as a temple servant at the temple is your own sentence.
It stands there as you have formulated it yourself and as others have understood and used it.
And if, like you just did, you don't think a thought error in a temple is right, then you can become a challenger.
Then you put something like your commentary with that little bit of adjustment on the altar.
And then temple servants and others can respond to this and perhaps adjust the thought.

In 2028, many people actually lived as if that sentence was true.
They wanted a lot of money, the nicest house and all that.
And they have given up a lot to be successful."

Sigur nodded.

Tin: "Everything is crooked if you only look at it from your own perspective.
On the other hand, everything is dead that can only be understood in foreign words.
That's why you will find your own words, your own description for the thought errors here in the Academy.
It's not enough that you repeat what someone else has said.
You will practice seeing everything from different angles and coming to thoughts that are alive instead of just having words.
There is no truth, there is only truth.
The most contradictory things can be true."

Tin eyed Sigur.

Tin: "I see you want to move on quickly now...
But you're not ready.
...you have to get past the plane sphinxes."

Sigur: "I know, Tin.
The plane sphinxes.
I am ready."

Tin: "You ready?"

Sigur: "I'm ready!"
He put himself upright in front of her.

Tin stared at him with worried eyes.

Tin: "What level do you want to go to?"

Sigur: "On what level is Lasse?"

Tin: "On Level 4."

Sigur: "Then to level 4!"

Tin nodded, "This is how it should be.
Level 1: Free software, encryption, distributed services and anything else that can stand up to any power in the world."

Sigur nodded back: "I know the 11 basic ideas of the Cypherpunks."

Tin: "I know, I know.
But knowing is not enough, you must understand.
Level 1 will be given to you.
You can always go there and visit the towns."
She threw a brightly colored glass ball into the air, which floated away and after a while broke up into thousands of tiny dust particles of all colors.
Sigur marveled at them laughing.

She looked at Sigur: "Level 2: Own ideals: justice, openness, equality, fraternity, instead of money, power and security."

Sigur nodded: "Freedom, let others share..."

Tin: "Level 2 ... is also given to you.
It is very rare that two levels are given as a gift. Of course, you can visit and explore it at any time."
She threw a different colored ball into the air, which also destroyed.

Sigur smiled.
His eyes were shining.

Tin: "Level 3: Overcoming opposition and everything else that overcomes the walls between people."

Sigur nodded.

Tin: "Level 3: The level sphinx is waiting for you.
Are you ready?" She looked at him forcefully.

Sigur: "Yes."

Tin fluttered her wings and in a flash she disappeared and the temple with the main square and the whole city and Sigur found himself in a dungeon without windows.
Coarsely hewn, damp stones everywhere, held together by mortar, lots of moss and a straw-covered stone floor.
There was a pungent smell of faeces.
Sigur held his nose.
He looked around in wonder.
He didn't know that.
Lasse hadn't told him about that.
He was thinking, walking around, looking at the walls.
The only light came from a tiny lattice door with solid iron bars at one end of the room.
It was just big enough that he could crawl out through it if it was open.
He checked the massive hinges, but found no weakness, even the lock was missing.
The grid was welded down.
He gripped the bars, shook them.
Impossible to move them even a millimeter.
He shouted loudly down the adjoining corridor.
No answer.
He tried to think his way back to the Cypherpunk Cafe, back to the bear, back to the practice city, but nothing happened.
Then he grabbed his head with both hands: "Oh no!
Not weird, not weird."

"Yes, I do!" said a voice behind him.
Sigur turned around.
A small rat looked at him with big, shy eyes.

He held both hands in front of his face and thought: "Oh, no!
I've hardly been here ten minutes and I'm already on Wunderlich.
Lasse has only been with him twice in his entire life!"

Strangely enough, yes, yes.
You messed up big time, you cypher punk!"
