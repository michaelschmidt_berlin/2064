## The Video
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">All pupils sat together in a large circle of chairs in the middle of the classroom.
The tables were stacked on top of each other on the wall.
Each had a piece of paper in his hand.

Kevin: "Can everybody know their lines?"

Everyone nodded.

Kevin: "Well, let's go!"

Lukas held a smartphone with a stabilizer and microphone on Kevin.

Luke pointed with his fingers: "Three, two, one..."

Kevin: "Dear Marlene!"

Lukas drove with the camera to Anni.

Anni: "We learned a few hours ago that you were working
international arrest warrant because you're
...hacked a vulnerability database."

Sophie: "We can all imagine that it was you here."
She pointed her thumb at the camera.

Aisha smiled, "Thank you for that!"

Moritz: "Thank you for having the courage to do this and risking your life for us."
He put his hand on his chest.

Viviane: "This is you!
That's how we know you."

Berem: "No one has ever intimidated you, although some teachers have tried."

Jannick: "We are all very proud of you here, everyone, without exception."

Lena: "And nothing will deter us from that!"

Felix: "Nothing!"

Antonia: "Not the nonsense that's being spread about you in the media..."

Janis: "... not some promise, like those made by the companies, not to install back doors anymore..."

Tom: "...nor threats from our teachers or anyone else."

Aylin: "Thanks for not letting me intimidate you!"

Leonie: "We will not be intimidated anymore either!"
She clenched a fist.

Lisa: "No matter what we do here, you were braver than us..."

Jonas: "and that's why we are now on strike here in the classroom until your picture is corrected in public so that we recognize you in it.

Antonia: "We miss you very much."

Marwin: "We could sure use your skills right now."

Jannik: "We didn't know all along that you were on the run.
We were told that you are on a world tour with your brother.
The teachers and your family told us so."

Nils: "None of that was right.
Just like what's in the paper today about you, which even Mr. Brunner defended in our room."

Philip: "We threw him out."

Devin: "Also the principal he came back with afterwards."

Marie: "We know that you haven't changed, and that the things you do are done because you are convinced that they have to be done.

Johanna: "Just as we are now doing what we think we have to do."

Nils: "We hope so much that you are doing well somewhere out there in the world."

Emilie: "That they won't find you and that you can keep doing what you want to do."

Tatjana: "We are with you, no matter what you are up to."

Kevin: "Marlene! We celebrate you here!"

Lukas: "Take!
Okay.
I got it.
Now everybody wave and hoot again."

Luke filmed everyone in a circle once again.

Lukas: "Super!
Two takes.
Out with it now!"

Kevin: "Wait!
Once more to me."

Luke held the camera on Kevin.

Kevin: "We are happy about all the other school classes who want to celebrate with us.
We celebrate that there are people who can really do something for our freedom on the Internet.
And maybe we, as school classes, can help."

Everybody was yelling again.

Luke: "Horny.
Final take.
Let's have it out now."

Kevin: "Does everyone agree?"

Anni wiped a tear from her eyes: "Yes, damn.
Send this out!"

Kevin: "Then get rid of it.
I just opened up a new Twitter account: @Marlene's class."

Lukas: "Super. I sent you the video."

Kevin pressed his smartphone a few times: "Okay.
It's gone.
WikiLeaks has it.
Let's see when they retweet it."
