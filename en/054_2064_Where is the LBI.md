## Where is the LBI?
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lasse and Sigur</span> were again sitting in the kitchen of the games laboratory.
Everyone again with a big cocoa in their hand.

Sigur: "And where are you right now?"

Lasse: "At the end of the game I was on a plane home to San Diego."

Sigur: "You got in?"

Lasse: "Yes.
In Caracas everything is controlled by Fortunato.
He's got a hell of an overview.
And I don't trust him an inch.
And then there's not much I can do there.
Rather from home, from our beach apartment.
I don't even trust him anymore that he didn't know about the robbery."

Sigur: "Nooo.
Of course he's fully involved.
That was an act of his.
But really, look, there are assholes like that: They cut open your stomach and put a remote-control cyanide capsule in it.
I'd love to know if this really happened 50 years ago.
I really got angry."

Lasse: "Unbelievable, if it were really the case.
Where are you now?"

Sigur: "Still in the room where the guards took me.
You've been ignoring me for a few hours.
Just stupid.
My head is also throbbing and I feel sick.
I feel the pressure in my stomach, gag reflex.
I feel a little nauseous in here... even here."

Lasse: "But what now?
What do we do with the LBI?
How do we look there?
Did we miss anything?
We don't know a lot of new things."

Sigur: "We try it through the TAO.
We still have two or three unused ports of access from the battle bot hack.
The TAO must have something to do with them."

Lasse: "Yes, it's really stupid that we have no idea where they are.
They don't even have buildings, no central servers, no place for us to go."

Sigur: "They use Tor network, they have their data hash networks and use Tails or Cubes OS as operating system.
They don't have anything better to own.
They need free software.
And then somehow we build a back door into Tails and we go into their computers through that."

Lasse: "Are you crazy?
A back door at Tails?
I think you're crazy.
First, it is against the Cypherpunk ethics of building back doors into standard programs.
And besides, you couldn't even do that.
The project is highly active, they look at everything new two or three times before they take it.
And you are not known there."

Sigur: "Hey, that was fun!
You know where I am.
How am I supposed to do anything?
Drugged, no computer, prussic acid in his stomach...
This is getting rather boring for me now."

Lasse: "Yes, it will be like with Tim.
He once spent two whole days in a psychiatric ward, in a mini room without windows, in a straitjacket.
Couldn't help it... Shit.
But I do not understand the logic.
Why did Marlene send us to Fortunato to find the LBI
And all that happens is that we hear about it and then..."

Sigur jumped up: "Just a moment!
Yeah, sure!
Fortunato, <em>er</em> is at the LBI.
He works for them.
He's LBI.
Of course.
We were in an LBI house.
How does he make such a transition from drug lord to information broker?
Without help?
His system built the TAO, I'm sure of it.
And he's not TAO.
He has a house to himself, all computers run privately.
This is an LBI house.
I'm sure of it.
I think we found it."

Lasse: "Shit ... Yes.
That makes sense.
That makes a lot of sense.
And we robbed them right away..."

He smiled.

Sigur: "Yes ... ...and stolen from me.
Wicked!
The Bitcoins should be on their way by now.
I'm sure he looked into what happened in his shop.
When you are at home, you will receive a Pond message from me after three days with the key to my Raspberry Cave.
This will be sent automatically if I don't hear from me for three days.
And by the looks of things, I suppose it will be."

Lasse: "Not too detailed.
Not that I'd be doing anything in the game that I couldn't do from the game."

Sigur: "You can assume that I have a dead man switch..."

Lasse: "Sure, of course."

Sigur: "And I assume that you would then search the Raspberry completely for clues ..."

Lasse: "Yes, that's what you do when a dead man switch sends something.
Okay... And then what happens?"

Sigur: "Then?"

Lasse: "What do we do then?"

Sigur: "That's a lot of money.
There are many billions, many billions... and all money outside the banking world.
These are Bitcoins.
I don't think they can do without it so easily.
I'm sure many people have to pay past the normal payment channels.
And they can't do that anymore, or at least much less well.
They'll be after the money... how stupid.
Bitcoins are not anonymous.
We must be careful how we spend it."

Lasse: "Don't worry, I won't spend it.
We use them as bait.
"We can get the LBI to come to us..."

Sigur: "That sounds cool."

He took his cocoa and took a big sip.

