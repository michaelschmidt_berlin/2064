## Class meeting
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">The whole class</span> had lively discussions in small groups distributed around the classroom.
Kevin gestured wildly in front of some of his classmates.
Anni and Sophie showed other Annis Smartphone and explained excitedly.
A teacher came into the room, stood in the middle in front of the blackboard, looked around and waited for calm.
After a while he shouted into the room: "Now there will be silence here!
Sit down!
Take your places.
All of them!
And I mean NOW!"

None of the students reacted.
The teacher took a deep breath, waited, looked, then shouted louder: "REST!
SILENCE NOW! TAKE YOUR PLACES!
What about you LOS?"

No reaction again.

Teacher loudly: "I know... I know what happened.
I know it!
Marlene was in your class.
I understand that this upsets you.
And that's a matter for discussion.
But everything has its time.
You can continue this after class during the big break."

Kevin turned to the teacher: "No, we can't do that in the big break.
It's your turn now.
That's more important than teaching."
The class became quieter.
Some turned to face the front.

The teacher stood with his legs apart in the direction of Kevin:
"I decide it happened here, not you, Kevin!
And we're going to take lessons!
Places, everyone.
And I mean now!
I won't say it again."

It became very quiet in the room and everyone looked at the teacher and at Kevin.

Kevin calmly replied: "No, you don't decide that.
This is more important.
Marlene's life is on the line.
And we'll keep talking until we know what we're doing for them."

Now it had become spookily quiet.
The teacher and Kevin looked each other in the eyes.
Kevin crossed his arms.

Teacher: "Kevin, you risk reprimand.
You already have one.
There's nothing we can do for Marlene.
She got herself into this and she has to see how she gets out of it.
It is prohibited to hack into other people's computers and steal secret data.
Whatever that is.
She wasn't an insider, she didn't work there.
So she's not a whistleblower either.
We live in a constitutional state and there are laws that must be observed.
Obviously Marlene didn't do that.
And breaks into the world's greatest intelligence agency, of all places.
Besides, people have died."

Anni excited, loud: "I DO NOT FASH IT!
I don't believe it.
They were on a school trip with us and Marlene.
You've known her almost 10 years, you know how Marlene thinks.
You know why she does these things...
Now you're parroting media shit.
That's pure propaganda.
They teach us to form our own opinions.
And you fall for such cheap manipulation yourself.
I don't do lessons here anymore.
Nope.
Not today and not tomorrow."
She folded her arms.

"I'm not doing any more classes either," Sophie said with a horrified expression on her face.
She shook her head.
"It's about Marlene.
No more lessons for me.
Nope.
Until this is cleared up."
28 students looked at the teacher.

Teacher: "I, um, I..."

Kevin calmly: "I don't do lessons anymore either.
Until the picture of Marlene is fixed and we don't hear shit like that again."

Teacher: "KEVIN! Kevin!"
He looked around the class.
"It's... in fourteen months you'll graduate from high school."

Kevin loud: "Fuck the A-Level!
This is about Marlene's life.
Don't you understand that?
They have the death penalty in the US.
Germany wants to extradite them.
Fuck graduation!"

Teacher: "Kevin!
You don't know what you're talking about."

Kevin pointed his finger at the teacher:
"Yes, I do.
And I mean that.
And you let the media shit in your brain.
It smells up to here."

Teacher breathed out and in: "KEVIN! GET OUT!"
He pointed his finger at the door.

Kevin didn't move.
The students around him demonstratively placed themselves in front of him.
The teacher looked at their faces one by one.
Everyone looked back determined and calm.
After a while he turned to the door and hurried out of the room.
She fell into the castle behind him with a loud crash.

Kevin stood in the middle of the class and looked towards the blackboard as if he was absent.
His classmates looked at him.
After endlessly long, quiet one or two minutes, he said determinedly:
"We are on strike!
We're making it real now.
Who's in?"

"Are you sure about this?" asked Tatyana, the best in the class, excitedly.
"I can't go along with this.
I can't do that."

"Absolutely." Kevin replied.
"But nobody has to join in, it's perfectly okay with me if someone doesn't want to join in.
But me, I can't help it."

"I'm in," cried Sophie.
"To Marlene!"

"Me too," said Anni.
"To Marlene!
But I want to celebrate Marlene too!
For what she did for us!"

Kevin: "Yes!
That's good.
Marlene celebrate and strike."

Lucas: "Awesome!"

Tatjana had packed her bag in the meantime and was on her way to the door.
She turned around again and looked at the class.
Then she put her bag down and said, "Fuck it, I'm staying too."
Kevin ran up to her and hugged her.

Anni stood in the middle in front of the blackboard: "Guys!
I have got an idea.
I got it the minute I read the article."
It got quiet.
"We'll send a message to Marlene.
We're making a video.
In it we tell the world what Marlene is really like.
People have to believe the media because they have nothing else.
Nobody knows what Marlene's really like.
We know that.
So we make the video and send it to everyone who wants to help us.
My neighbor's blogging.
That would definitely do the trick."

Lukas: "We send this to WikiLeaks and to the Courage Foundation.
I bet they tweet that.
You have nearly six million followers."

Kevin: "Good idea.
I'm sure they know Marlene well, if she's not even in on this.
Who does the script?"

"Sophie!" said a boy.
"Yes, Sophie", another one.

Sophie: "Okay, but you have to help me."
She took her bag, sat down at the table in the back corner of the window, unpacked her writing things and began.
Kevin followed her and sat down on the table in front of her.

The door bursts open.
The rector stood in the entrance with wide legs and crossed arms.
Behind him, half-hidden, the teacher.

The headmaster thundered: "What kind of a mess is this?
This class again!
I told you I was keeping an eye on you.
I told you that last time."
Three or four students turned to him.
"After the irresponsible tree climbing, where the whole class was together in a tree, and then after a teacher quit because of you..."

One student: "She was totally incompetent."

Principal: "Shut up!
I'm talking now!
After a teacher quit because of you, and we talked, I thought that peace would come.
And now this!
Mr. Brunner has given you clear instructions.
You have disobeyed her..."

One student said, "That was Kevin. Kevin disrespected her, not us."

Principal: "This is not possible at our school, not in this time.
Teachers have a great responsibility, and therefore they must be able to say what is happening here.
And this must be followed.
Absolutely.
Or else chaos will break out here."

A second student: "Do they also bear responsibility for Marlene?
There are people who want to kill her."

Principal: "Hell!
Marlene Farras is no longer at this school!
We can't be responsible for everyone, especially not for someone who is having fun somewhere in the world and then simply hacks into military databases.
She knew what she was doing.
This didn't just happen to her."
He looked into the corner where Sophie was writing the letter with Kevin.
"KEVIN! Will you come here, please?"

Kevin didn't take any notice.
He was engrossed in a conversation with Sophie and did not care about what else was going on in the class.

A second student to the headmaster: "Short update: She was not having fun, she was on the run.
You told us all along she was having a good time.
That was a great shit.
Extreme shit."

Second student: "For me, what you say about Marlene now is all super incredible."

The headmaster first looked irritated at the students, then at Kevin and Sophie: "KEVIN!
ARE YOU COMING HERE NOW?
I won't say it again."

Anni: "You don't have to say that again.
He's not coming anyway."

The headmaster gasped for breath.
He looked briefly at the teacher, then at the class.
Then he wanted to go to Kevin, but Anni and some other students got in his way.

Principal: "Let me through!"

Anni stood directly in front of him: "No!
He works with Sophie.
You can see that.
He's doing something important.
And you're not disturbing him now!"
Behind her the group moved closer together.
The headmaster opened his eyes, looked threateningly at Anni, then at Kevin, then at the group, then again at Anni.

Luke from the side: "You once told us that we should learn to take responsibility.
We do now.
Because the teachers don't seem to be doing it.
Marlene's life is in danger.
Don't you get it?
It's not a game anymore!"

The headmaster grabbed Anni by the shoulder in anger and wanted to push her aside.
She blocked.

The headmaster shouted a hand's breadth from her face: "GET ME OUT OF THE WAY!
This has consequences for you already.
Don't make it worse."

Anni straightened up and looked him in the eye: "NO!" She shook slightly.

Luke took the hand of the rector with strength from Annis' shoulder: "This is not possible.
Teachers at this school are not allowed to grab students by the shoulder."

The headmaster pulled out and slapped Lukas so hard that his head flew to the side.
Luke held his cheek, gasped for breath and looked at the headmaster with amazed eyes.
He held his breath and looked back.
Luke took a step forward, turned his head and said calmly: "Do you want to hit the other side too? Motherfucker!"

The headmaster blushed.
He tried to say something, made a hand movement, but then turned around and left the room, the teacher followed him.
