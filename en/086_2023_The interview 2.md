## The interview (2)
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<The hostess looked at Marlene, "Yes!"

She smiled and nodded at her.

Moderator: "Welcome, Marlene.
These things actually happen.
You are the first guest here who is also a surprise guest for me.
I didn't know anything about it until yesterday.
My heart is pounding a little bit.
For here sits the President of the United States and here sits the most famous hacker in the world, who has given us all so many lasting experiences over the past year, and who is still wanted with an international arrest warrant.

How many times since the vulnerabilities were released, we've had to change our passwords, remember new ones because the old ones were rejected by Google, Apple, Facebook and whoever else.
And each of us knows someone whose smartphone, WhatsApp account or email has been hacked.
Some accounts have disappeared completely, others are completely devastated.
A lot of money disappeared and most of it never came back.
Companies have gone bankrupt.
And we all learned something about passwords, backdoors and encryption and why these things have more meaning in life than we ever thought. Marlene, how are you?"

Marlene took a short break, then laughed, then started again: "Yes, well, actually.
I have enough food, good friends and usually a roof over my head.
The only thing is that I have to be embarrassingly careful that the CIA doesn't find me, the BND or Academi or whoever.
"But that's what happened now. She took a quick glance at the camera.

Moderator: "First of all you wanted to say something about what you are going to do after the show, how you have planned your _home way_, if I may say so.

Marlene: "I won't say much about what we have planned.
I just want the Twitter name: @MarleneUndLuigi here.
If possible, we will let you hear something about this every 10 to 30 minutes.
It would be nice if there were as many of them as possible."

Moderator: "Okay.
@MarleneUndLuigi, please write them all down.
Who is Luigi?"

Marlene shook her head.
"A friend to help me."

Moderator: "Good.
So, then we get down to business.
You, like every surprise guest, brought three questions?"

Marlene nodded.

Moderator: "Then you can ask them now."

Marlene: "Okay." She nodded at the President.
"Then I start?"

The president nodded back.

Moderator to the President: "You may refuse to answer one question.
A wild card, so to speak.
There will be a supplementary question."

Marlene pulled out a piece of paper and read: "First question.

<div class="handwritten">
What do you think is more important in a democracy:
The right of responsible citizens to know the things that concern them?
By this I mean everything they need as responsible citizens to make democratic decisions, for example what the state does to ensure their security.
Or on the other hand, the secrecy of such things for tactical reasons, so that they are not exploited by enemy forces?
</div>

The president laughed, "Oh, yeah.
That's a good question...
As much of both as possible.
It's a contradiction, I know.
But we need secrecy.
The forces out there in the world are not all good.
And I think I see more of the bad things there as president than most people.
And I want to see them so that I can make decisions based on how the world really is and not how I imagine it to be.
We must not let these forces rage freely.
We have to do something about it for our protection.
And this can only be done in secrecy.
If we always said what we were planning and what we were doing, we would be very easily predictable.
It's probably a bit like what you've been doing for the last two years.
You also kept your whereabouts secret.
But the letter you wrote to your class, you didn't have to keep it a secret.
I let everyone read it.
So there are things that have to be kept secret and things that can be said publicly.
And in the democratic process we have to decide which ones are.
The responsible citizens determine through their elected representatives which information should be public and which secret.
And the secret ones will eventually become public.
For example after 30 years.
Then it's part of history and it can become public."

Marlene: "I'd rather say after four to six weeks."

President: "Would you reveal your whereabouts after four to six weeks?
Then you have to leave there and you can't go back there either."

Marlene swallowed and said after a few moments, "Four to six weeks after I leave there, after it's over." She took a break and then said slowly: "Berlin, Dahab, George Town, Nairobi, Addis Ababa and ... Luino." Her heart started pounding.

The President swallowed.
Both looked into each other's eyes.

Marlene: "Second question?"

The President nodded.

<div class="handwritten">
If a hacker makes vulnerabilities public,
that he or she found out in a hack and is wanted for it internationally,
would you, in a presidential pardon, revoke his or her international arrest warrant?
<br /><br />
In this particular case, would you cancel my arrest warrant?
</div>
Host: "Stop!
Marlene, this is a personal question.
It's against the rules of this show.
You read them."

Moderator to the President: "You don't have to answer that."

President: "I shall reply to that.
Then I'll have two questions behind me." She smiled warmly.
"I will have your warrant examined.
I think that's what's required.
If there is really nothing else than the hack into the NSA database, anything that has to do with the vulnerability database, then I will make a recommendation.
I must not interfere with the American legal system, that would violate its independence, and I can only express a pardon after you have been convicted.
But in any case, I will make it clear from the outset that I am willing to do so if a conviction is reached.
If that's the only thing that's against you."

She gave her a serious nod.

Applause and occasional jeering in the audience.

President: "For me, this is also a democratic decision.
You have the support of a large majority of the local population."

Marlene nodded and smiled at the president.

Marlene: "My third question

She hesitated and glanced briefly at the president, taking a breath.

<div class="handwritten">
If I had a brother,
who was arrested for my hack and has been in a secret CIA prison for two years now,
without him having anything to do with it,
who's probably just being held,
because he's my brother,
would you help me get him out?

And if...
</div>
The presenter walked in between, annoyed: "Marlene!
These are personal questions.
We said no personal questions.
Madam President, I would suggest we take the supplementary question.

Marlene: "I have no further questions."

In the meantime Linda stood chalk-white before the curtain, so that she was almost seen by the audience.
She put both hands in front of her face.
The president looked over at her almost at a loss.
The audience became restless.

Moderator: "Marlene, do you really not have a supplementary question?
We talked about this."

Marlene shook her head.

The president turned to her: "Are you sure he's in a secret CIA prison?
There's a lot of false information and wrong lists that are floating around the internet."

Marlene nodded: "Absolutely.
The list is direct from the CIA, and up to date.
She pulled out a piece of paper and shoved it across the president's desk."

President looked at the paper, "How did you get this?"

Marlene: "A hack."

The president ran red in the face, closed her eyes and held her breath.

Marlene: "Not from me.
I didn't do it.
I didn't hack that."

President ernstster: "Then you are not sure that this is real.
The list is almost certainly a fake."

Marlene: "No, she's real.
You could check.
You can just call them and ask."

The president took a deep breath, looked at Linda: "I will have this checked.
First thing tomorrow."

Marlene: "Okay." She looked at the president calmly. "I understand."

President: "Or, wait." She turned back and called Linda to her table.

President to moderator: "This is Linda, my intercultural relations consultant."

She gave the sheet to Linda and whispered in her ear, "Is it possible to verify such information immediately?"

Linda shrugged, "Probably."

President: "Can you check them?
And figure out what options we have?"

Linda nodded carefully, took the list with slightly trembling hands and disappeared with it behind the curtain.

"My advisor will look into it."

Applause in the audience.

Hostess: "Wow.
That's brave.
That was very quick.
It's really all live here."

She threw an admiring glance at the president.

Moderator: "So fast. Hmm. "I suggest we take viewer questions in the meantime..."

She got up, went to the front of the stage and read from a sheet.
"Should only open and free software be used in Germany's schools in general?
What do you think?"

About twenty minutes later a loud conversation between Linda and a man could be heard behind the curtain.
Then Linda pushed the curtain aside and went to the president as calmly as she could.
She began to whisper in her ear.
The president nodded from time to time, whispered something back, listened again.
Meanwhile the presenter asked the audience another question.
After a very long three or four minutes, the president said so loudly that Marlene and the moderator could hear it: "This is how we do it." Linda nodded to her and disappeared behind the curtain.
Everyone looked at the president with excitement.
It got quiet.
She turned to Marlene.

President: "Marlene.
It's actually true.
You're right.
It's embarrassing that the CIA can't keep its secrets to itself, but in this case there is something good about it.
Her brother is in a Virginia jail and no charges have actually been brought against him.
We even had the case already on our list, and had already looked at it.
In my view, it is clearly one of the cases in which the secret services have exceeded their powers.
We want to correct these cases, and have been doing so for some time.
Not publicly, of course.
I will make this case an example of how we deal with things differently.
As I have said, I want to convince by actions and show that our legal system is very lively and powerful."

Loud applause from the audience.
Marlene beamed all over her face, poured her head, then looked at the president.

President: "There are still a few things to be discussed, however.
But it's best if you do it directly with Linda.
She has an idea.
You want to see your brother real quick."

Marlene instinctively jumped up, walked around the table and hugged the president.
Two security guards, who could not be seen before, jumped in from two sides and held her by the shoulder.

Marlene smiled at the president and then looked towards the curtain.

Hostess: "Yes, go..."

Moderator with a hand movement towards Marlene: "Marlene Farras!
Thank you!
Thank you.
We're not going to forget this anytime soon.
Thank you very much. And good luck!"

Marlene looked once more into the clapping audience, waved and then disappeared behind the curtain.
