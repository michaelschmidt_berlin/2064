## More Whistleblowers
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Kevin stormed into the classroom.

Kevin: "Quick! Google. Facebook. Amazon. Apple."

Luke sat down in front of his computer and typed.

Luke: "What?"

The Google website appeared with a black background.

Kevin: "They did it. You really did it."

Anni shouted from afar: "Apple is black too."

Another student: "And Facebook."

Luke: "Shit.
Everything on Google is black.
Not just the home page."

Kevin put a hand on Luke's shoulder: "Horny.
There, click the button!"


<div class="terminal">
AMAZON - APPLE - FACEBOOK - GOOGLE - MICROSOFT - TWITTER - DROPBOX
<br /><br />
JOINT OPINION
<br /><br />
As a technology company, we have so far concentrated on what we do best:
Offering as many users as possible the best technology experience available.
At the same time, we have always endeavoured to observe the legal framework conditions that apply in the respective countries.
These are very different.
In China, different rules apply than in Russia, in the U.S.A. different rules than in Europe.
<br /><br />
In our home country we naturally had closer ties to government agencies. We had the confidence that this would not be exploited.
We have set up direct access options to user data.
There were hundreds of thousands of requests for specific accounts. We never analyzed them.
<br /><br />
After recent events, we have now done so. What did we find out?
Only a very small percentage of this was actually attributable to homeland security.
The bulk was marked as inquiries about criminal cases.
We already knew that.
What we did not know was that the vast majority of criminal cases were merely suspected cases.
No charges were brought, nor were any proceedings in progress.
The mere expression of a suspicion was sufficient reason for a request.
This is an abuse of the access systems we have provided.
We have now finally shut down and deleted these systems.
<br /><br />
As of today, we will no longer follow any instruction that again requires direct access.
Requests must be made in writing by post. We will also automatically inform the affected user about the access six weeks after each request.
<br /><br />
We have activated a website:
<br /><br />
www.nsl-and-gag-orders.com
<br /><br />
There everyone can see what instructions we are currently receiving from the government.
NSL means National Security Letter.
This contains the actual instructions, for example, to install a vulnerability.
GAG orders are instructions for secrecy.
They threaten the disclosure of NSLs with jail time.
We are doing it anyway because we are convinced that we have to do it now.
<br /><br />
We hope for your support.
</div>

In the meantime Anni, Sophie and Oskar had come to Luke's computer and read along.
Other students were added.

Oskar: "This is real.
It's not open source yet and transparent insight into their server systems, but it's real."

Kevin: "Wow."

Luke: "I'm exhausted."

Sophie: "I find it understandable that they do that.
What are they supposed to do, according to _the_ statements of the NSA director?"

Annie: "Companies become whistleblowers! That's awesome."

Oskar: "Right, _that_ is the only thing that counts here:
They publish live.
And because they still have whistleblowers in their ranks, they have to publish everything.
Because if they report anything new without anything moving on this website..."

Kevin: "If our boys and girls report anything new at all ... You've basically said that CEOs are more likely to go to jail than to continue to do what the intelligence agencies want."

Annie giggled: "Right now I would like to go to prison as CEO.
That would be nice.
Imagine: Larry Page in handcuffs for publishing a national security letter."

Oskar: "And 10 minutes later pardoned by the president."

Everyone laughed.
