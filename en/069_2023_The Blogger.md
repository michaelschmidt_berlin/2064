## The Blogger
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Amelie</span> took her cup out of the coffee machine in passing and sat down with verve behind the studio table.
There was a tablet there showing a photo of Marlene Farras in her class.
She looked at it and smiled.
"Joe!" she shouted to the cameraman, "what's your percentage today?"
"102," he called back while checking the camera.
Amelie laughed, "I know exactly what those two are..."
Joe smiled at her.
The studio was a three-room apartment in Kreuzberg, on the 3rd floor of an old apartment building.
Amelie has been blogging from here for almost four years, beauty tips first, then other topics such as jewelry, smartphones, and sometimes yoga or vegan food.
Everything "Out of Berlin" that interested her.
She had built up quite a large fan base with it.

Last year, hacks were also a frequent topic due to the publication of the vulnerability database.
Hardly any of her friends had not had problems: either with their Facebook accounts or with Instagram or YouTube.
Once she could not broadcast for three weeks because a youth from Bangladesh had taken over her YouTube account, despite 2-factor authentication.
Thank God she already had over 2 million subscribers and so she got the account back from Google relatively quickly.
Others had been less fortunate.

"Where's my manager?" cried Amelie into the room.
She looked around.
In the left corner Freddy, a lanky Twen, was working on a spotlight, on a sofa in the middle sat Anni, behind a mixing desk on the right side Sven, with a serious look and two huge headphones.

Anni shook her head: "You don't have one.
You mean the scriptor."

Amelie: "I mean anybody here with a plan for what comes next."

Anni: "Nobody here has a plan what comes next...
Ameliiieee!
You know that."
She laughed.

Amelie: "Okay.
We have no plan.
Then everything's fine.
I just wanted to make sure.
Then let's get started!"
Joe turned the camera towards her.
Freddy aimed the spotlight.
Sven raised his thumb.

Amelie: "WELCOME! At Amelies Out of Berlin.
Today for once we have no real plan what is coming today ...
WHO just said it's always like that? ...
Okay.
But today we have a reason why we can't have a plan, we couldn't even if we could.
Something happened tonight that needs to be discussed here.
And this to our big series: Weak point database, huaaahhh.
The big issue of data security... or how to keep assholes off my smartphone.
The day before yesterday we showed you the fantastic video of Marlene's class and Anni explained the background.
Anni, from Marlene's class. You remember. Anni!"
Amelie pointed at Anni.
The cameraman panned to the sofa.
Anni waved briefly into the camera.
Amelie: "Great video.
Important message.
Celebrate and strike.
Yeah.
And tonight... came... THE ANSWER!
Yeah. by Marlene.
Marlene answered.
And we're sure it was her and not some fake.
Probably from a completely different time zone, because the answer came at about 3 a.m.
Nobody knows where she is.
I read it right away, we set alarms on something like that, and saw right away that we needed a real hacker to translate it for us.
Yeah.
And it should be behind the front door now.
I told him: 1:03 and 30 seconds.
Then let him come in.
28 ... 29 ... 30.
TA DAAAA."

The camera panned.
The door remained closed.

Amelie: "Hmm.
I thought hackers and programmers loved accuracy.
Annie?"

Anni shrugged her shoulders.
She got up, went to the door and was just about to open it when it suddenly jumped up and Oskar was standing in the room.

Amelie: "Aha!
There you go!
Welcome Oskar!
Oskar from WikiLeaks.
Come over here."

Oskar walked towards Amalie with a broad smile and hugged her.

Amelie looked at Oskar, breathed in and out, and said, "You'll feel a lot safer with a hacker around."

Oskar smiled again.

Amelie: "Oskar!
Something happened tonight, didn't it?"

Oskar nodded.

Amelie: "Marlene wrote a reply to the video.
For the first time ever something for the public.
Why not a video?
And where on earth is she when the news arrives at 3:00 a.m.?"

Oskar: "When the message comes means nothing.
Marlene surely sent them with a delay of 3 - 9 hours.
So you don't know where she is in the world.
That was her big decision not to go out in public after the hack.
It's a lot more dangerous, but give her more freedom.
She's still looking for her brother."

Amelie: "It is more dangerous for her not to go public?
Why?"

Oskar: "Very simple.
Publicity, awareness, it's like life insurance for hackers.
The more famous you are, the more people look at you when you suddenly disappear.
Some hackers have simply disappeared or died in a strange way, like Boris Floricic here in Berlin, Tron."

Amelie: "I don't know him."

Oskar: "Exactly.
And then the media are not interested in it either.
This was a very big deal in hacker circles.
But today there are only a handful of people who know about it.
Tragic story.
At least some of it's on Wikipedia."

Amelie: "Okay...
Then we bloggers can contribute something important to this struggle: we can issue life insurance policies."

Oskar: "Absolutely.
Absolutely.
You could become a life insurance for Marlene if you regularly tell a few million people about her and they find it exciting.
But Marlene would need a huge reputation by now to be sure.
So maybe if all YouTube bloggers together would do a longer action.
Aaron Schwartz was already quite famous.
He still didn't make it.
But at least it was more expensive for the people who wanted him... gone:
He's got Wikipedia just saying he committed suicide."

Amelie: "Wow.
It's really a matter of life and death with you guys.
Wow."

Oskar: "Absolutely.
For Marlene: super real."

Amelie: "All the more astonishing that I can perhaps help to save her life with a beauty blog.
This is exciting.
Yeah.
I can imagine that I do."
She turned to the camera.
"Girls!
Boys!
And everyone else!
We're gonna start saving Marlene's life right here and now...
Together.
Okay?
And for those who disagree, find another blog..."

She took the tablet, started a photo editor, and cut Marlene's face out of the image that the tablet displayed.

Amalie: "So, I now have a picture of Marlene and I share it here in the blog.
There it is!
And everyone of you who had any problems with the vulnerability database, or just has a neck because of all the people who are constantly adding new backdoors: send this to a copy shop and have it printed 10 times.
AIN-A3, color.
Ten is good.
And you also send the file to other people who were also involved with the vulnerability database.
We were all in on it anyway.
And then you all make yourselves free the day after tomorrow evening, that's Tuesday, free -- it's about saving a life, you can take some time off -- and then you find 10 nice places for the pictures.
Somewhere where a lot of people can see it.
And where it's gonna stick for a while.
You may have a little bit of courage.

We're saving Marlene!
HEJ!
It's still the least we can do for the woman who risked her life to show us how many assholes crawl around in our smartphones, right?"

Oskar laughed and held his Raspberry Pi out to Amalie: "I've already sent it to the copy shop..."

Amelie looked at the screen of the pis: "What is that?"

Oskar: "A kind of smartphone where I still know the inner workings to some extent.
With Linux.
Do you know this?"

Amelie: "This is one of the things Marlene recommended for her class?"

Oskar: "Yes, you could say that.
It's one of the things that Marlene's class already has because a few hackers are taking care of her digital welfare."

Amelie: "I want one of those..."

Oskar: "Hmm. Poor resolution, pretty slow, no Android or iPhone apps..."

Amelie: "But freedom..."

Oskar: "Jo, more freedom!
That's right.
And you can chop with it..."

Amelie: "Linux instead of Android or iOS.
And Linux wasn't affected by the vulnerability database, was it?"

Oskar: "Yes, yes.
But Marlene sent the Linux people the vulnerabilities four weeks before the release, then they were able to close the most important gaps before things really got started".

Amelie: "Okay. Open source! Free Software! We need it! I learned that."
Oskar nodded.

Amelie: "Oh yes, and you can find the letter from Marlene in the links.
There are more tips like that in here.
What are the tips, Oskar?"

Oskar: "The tips come straight from the front.
These are good.
But that's also what we're doing in class now."

Amelie: "I think I'll have to drop by there!"

"Yaaaa! Come over!" Anni called from behind.

Oskar: "Come by whenever you want!
We can also use life insurance there ...
Amelie, may I say something here?"

Amelie: "Sure, your camera."

Oskar turned to the camera. "Even if there are some students watching..."

"Plenty," Amelie cried in between.

Oskar: "When you go to school tomorrow, why don't you talk to the others about supporting Marlene's school class.
We are currently building a more secure chat system that will also allow video chatting.
Through the chats, some hackers offer courses on how to communicate more securely, or program, or hack, or a bunch of other cool stuff.
And if you prefer to do this rather than teaching, if you think it makes more sense to learn how the internet works, then celebrate and strike with us.
We'll take back the Internet together!"

Amelie: "Wow! That was a lot of politics for that blog..."

Amelie and Oskar looked at each other.

Oskar spread his arms and asked, "Hmm?"

Amelie turned to the camera: "Say what you think about it in the comments.
All I can tell you is that hackers are very cute people."

She hugged Oskar and cuddled up to him.

Amelie inhaled and exhaled again: "Uh-huh.
You have to try it."

Oskar carefully put his arms around her.

Amelie: "And remember: Send the picture to the copy shop.
The day after tomorrow we'll be glued!"

Amelie waved at the camera.
Oskar did the same.

Amelie: "See you soon."
Amelie made a kiss.
