## Spaghetti Milanese
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Marlene and Luigi</span> sat on the terrace in front of their laptops and listened intently to the last words of the president.
They looked each other in the eyes for a moment.
Then they both pulled up their arms and laughed.

Marlene: "Yeah!
Yes!
That's really something!
Luigi!
The mountain has moved!
We're on, we're on, we're on..."

Luigi: "Oh, oh, the secret services will not like that.
But it was inevitable.
They are messing with the top companies that America needs at least as much as the secret services.
And the companies are now really sore because of our girls and boys.
Julian Assange already foresaw this in the early 2000s: Whistleblowing makes the breakthrough."

Marlene: "And in addition the secret services get to do it now also still with Amelie...
She's really picking up speed.
Joan of Arc is back!
Yes, now something is possible.
Now we have to keep at it.
What the President said is gonna cause confusion.
Total chaos in the intelligence community.
You're not gonna take this lying down.
They'll respond.
You're getting careless.
That's where we'll get our chances.
We must provide Amelie with all we learn."

Luigi: "In any case, there will now be a bunch of new vulnerability reports!
We may get real, regular vulnerability updates from internal people at the central companies.
How awesome ..."

Marlene: "Yes, that's cool!
The big companies, the governments, they are so dependent on us little people.
They need us as consumers, as workers, as accomplices.
Without us nothing works.
We can dictate what they have to do.
Marwin used to say that.
You can do nothing against free programmers, free admins and free hackers.
It's so obvious now.
So horny!"

She put her face in her hands.
"Horny... awesome Marwin... I wish he could see that. - --Shit.
Maybe he won't even notice."

She closed her eyes.
After a while she got up and walked towards the railing.

Luigi: "Marlene!"

Marlene: "Good, good.
I'm not going all the way to the front."
She sat down cross-legged on the floor.
"Where is he now?
I want to know where he is now."

Luigi: "You went through all your contacts for the last time three days ago.
He's in America, somewhere in America.
Most likely a CIA prison.
There's no news on that.
But try again."

Marlene: "Eventually I'll find a lead."
She got up, sat in her chair, took her laptop and started typing.

Luigi: "I'm making spaghetti Milanese.
With Parmigiano-Reggiano and a touch of pepper.
With a Valpolicella rossa."

Marlene did not respond.
She typed and looked spellbound at her screen.
Luigi disappeared into the apartment.

Marlene said to herself after a while, "No wine for me."
