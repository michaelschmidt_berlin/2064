module.exports = {
    metadata: {
        outputProfile: 'kindle',
        authors: 'Michael Schmidt',
        bookProducer: 'Michael Schmidt',
        comments: 'Licensed under the Creative Commons Attribution-Non Commercial-Share Alike 3.0 license',
        isbn: '978-1660692392',
        pubdate: '2020-02-13',
        publisher: 'Michael Schmidt',
        title: '2064, The Cypherpunks Tale'
    },
    lookAndFeel: {
        extraCss: `${__dirname}/../media/2064.css`,
        embedAllFonts: true,
        lineHeight: 22,
        changeJustification: 'justify'
    },
    txtInputOptions: {
        formattingType: 'markdown'
    },
    mobiOutputOptions: {
        mobiFileType: 'both'
    },
    epubOutputOptions: {
    },
    azw3OutputOptions: {
        prettyPrint: true
    },
    pdfOutputOptions: {
        paperSize: 'a5',
        pdfPageNumbers: true,
        prettyPrint: true,
        pdfDefaultFontSize: 13,
        pdfAddToc: true,
        pdfHyphenate: true,
        pdfOddEvenOffset: 10,
        pdfPageMarginTop: 48.0,
        pdfPageMarginBottom: 48.0,
        pdfPageMarginLeft: 72.0,
        pdfPageMarginRight: 72.0
    }
}
