
const fs = require('fs');
const modules = fs.readdirSync(__dirname);

module.exports = modules
    .map(mod => mod.replace('.js', ''))
    .reduce((acc, mod) => ({
        ...acc,
        [mod]: module !== 'index.js' ? require(`./${mod}`) : null
    }), {});
