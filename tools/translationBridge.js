const fs = require('fs');
const commander = require('commander');

commander
    .version('1.0.0')
    .option('-p, --prepare', 'Prepare for translating')
    .option('-s, --split', 'Split text after translating')
    .option('-f, --inputFile', 'File to be prepared or split')
    .parse(process.argv);

const inputFile = commander.inputFile || `${__dirname}/tmp/translate_2064.txt`

const prepareFile = () => {
    const path = `${__dirname}/../de`;
    const textDir = fs.readdirSync(path).filter(name => name.endsWith('.md'));
    const dictDir = fs.readdirSync(`${path}/dictionary`).filter(name => name.endsWith('.yaml'));

    let bookText = '';
    for (var name of textDir) {
        const text = `${fs.readFileSync(`${path}/${name}`)}`;

        bookText += `{{{md§§§${name}§§§${text}}}}\n\n`;
    }

    for (var name of dictDir) {
        const text = `${fs.readFileSync(`${path}/dictionary/${name}`)}`;

        bookText += `{{{yaml§§§${name}§§§${text}}}}\n\n`;
    }

    fs.writeFileSync(inputFile, bookText);
}

const splitFile = () => {
    const regexChapter = /{{{(md|yaml)§§§?([^§]+)§§§?([^{}]+)}}}/gm;
    let regexArray;
    const bookText = `${fs.readFileSync(inputFile)}`;
    if (!fs.existsSync(`${inputFile}_/dictionary`)) {
        fs.mkdirSync(`${inputFile}_`);
        fs.mkdirSync(`${inputFile}_/dictionary`);
    }

    while ((regexArray = regexChapter.exec(bookText)) !== null) {
        const fileType = regexArray[1];
        let fileName = regexArray[2];
        const fileData = regexArray[3];

        fileName.endsWith('fileType') ? fileName : `${fileName}.${fileType}`;
        if (fileType === 'md') {
            fs.writeFileSync(`${inputFile}_/${fileName}`, fileData);
        } else {
            fs.writeFileSync(`${inputFile}_/dictionary/${fileName}`, fileData);
        }

        console.log(`Writing '${fileName}'. Next entry: ${regexChapter.lastIndex}`);
    }
}

(async () => {
    if (commander.prepare) prepareFile();
    if (commander.split) splitFile();
})();